package com.cubixsoft.mrmahircloneapp

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cubixsoft.mrmahircloneapp.activities.AddAddressActivity
import com.cubixsoft.mrmahircloneapp.activities.LocationActivity
import com.cubixsoft.mrmahircloneapp.adapters.AddressAdapter
import com.cubixsoft.mrmahircloneapp.adapters.DomainAdapter
import com.cubixsoft.mrmahircloneapp.base.BaseActivityWithoutVM
import com.cubixsoft.mrmahircloneapp.databinding.ActivityMainBinding
import com.cubixsoft.mrmahircloneapp.models.MainDomainResponse
import com.cubixsoft.mrmahircloneapp.models.PointsResponse
import com.cubixsoft.mrmahircloneapp.models.addresses.AddressDataModel
import com.cubixsoft.mrmahircloneapp.models.addresses.MainAddressResponse
import com.cubixsoft.mrmahircloneapp.models.domain.DomainDataModel
import com.cubixsoft.mrmahircloneapp.services.ApiClient
import com.cubixsoft.mrmahircloneapp.services.Constants
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.navigation.NavigationView
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : BaseActivityWithoutVM<ActivityMainBinding>(), AddressAdapter.Callback {
    private var drawer: DrawerLayout? = null
    var logo: ImageView? = null
    private lateinit var apiClient: ApiClient
    var listModel: List<DomainDataModel>? = null
    var addressList: List<AddressDataModel>? = null
    lateinit var navController: NavController
    var bottomsheet: BottomSheetDialog? = null
    var userid: String? = null
    var savedAddress: String? = null
     var dialog : Dialog?=null
    var userCityName: String? = null
    var navigationView: NavigationView? = null
    override fun getViewBinding(): ActivityMainBinding =
        ActivityMainBinding.inflate(layoutInflater)

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    @SuppressLint("UseCompatLoadingForDrawables")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)
//        Utilities.saveString(this, "userCityName", cityName)
//        Utilities.saveString(this, "userLatitude", mLocation!!.latitude.toString())
//        Utilities.saveString(this, "userLongitude", mLocation!!.longitude.toString())
        val profileImage: String = Utilities.getString(this, Utilities.USER_PROFILE_IMAGE)
        val userName: String = Utilities.getString(this, Utilities.USER_FNAME)
        userid = Utilities.getString(this, Utilities.USER_ID)
        savedAddress = Utilities.getString(this, "savedAddress")
        Toast.makeText(
            this,
            "Welcome " + userName, Toast.LENGTH_LONG
        ).show()
        if (!TextUtils.isEmpty(profileImage)) {

            Picasso.get()
                .load(Constants.BASE_URL_IMG + profileImage).placeholder(R.drawable.profile_ic)
                .into(mViewBinding.profileImage)

        } else {
            Picasso.get()
                .load(R.drawable.profile_ic).placeholder(R.drawable.profile_ic)
                .into(mViewBinding.profileImage)

        }
        val address: String = Utilities.getString(this, "userAddress")
        userCityName = Utilities.getString(this, "userCityName")
        mViewBinding.apply {
            tvAddress.setText(address.toString())
            rlHomeSrvices.setOnClickListener {
                startActivity(Intent(this@MainActivity, HomeActivity::class.java))
            }
        }
        apiClient = ApiClient()
        if (savedAddress.equals("yes")) {
            callApi(userCityName)
            getPointsApi(userid)
        } else {
            showBottomDialog()
        }


    }
    override fun onResume() {
        super.onResume()
        val profileImage: String =
            Utilities.getString(this, Utilities.USER_PROFILE_IMAGE)
        if (!TextUtils.isEmpty(profileImage)) {

            Picasso.get()
                .load(Constants.BASE_URL_IMG + profileImage)
                .into(mViewBinding.profileImage)

        } else {
            Picasso.get()
                .load(R.drawable.profile_ic).placeholder(R.drawable.profile_ic)
                .into(mViewBinding.profileImage)

        }
    }

    fun callApi(cityName: String?) {


        apiClient.getApiService().getDomains(cityName)
            .enqueue(object : Callback<MainDomainResponse> {
                override fun onFailure(call: Call<MainDomainResponse>, t: Throwable) {
                    Toast.makeText(
                        this@MainActivity,
                        "failure case:   " + t.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()
//                    tvStatus.visibility = View.VISIBLE
                }

                override fun onResponse(
                    call: Call<MainDomainResponse>,
                    response: Response<MainDomainResponse>,
                ) {
                    val loginResponse = response.body()

                    if (loginResponse?.statusCode == true) {
                        listModel = response.body()!!.getData
                        mViewBinding.rvList.apply {
                            setHasFixedSize(true)
                            layoutManager =
                                GridLayoutManager( this@MainActivity, 1, GridLayoutManager.VERTICAL, false)
                            adapter = DomainAdapter(
                             this@MainActivity, listModel
                            )
                            mViewBinding.shimmerViewContainer.visibility=View.GONE
                            mViewBinding.rlLoyalityPoints.visibility=View.VISIBLE
                            mViewBinding.rvList.setHasFixedSize(true)
//                            mViewBinding.tvStatus.visibility = View.GONE

                        }
                    } else {
//                        val loginResponse = response.body()
                        if (loginResponse != null) {
                            Toast.makeText(
                                this@MainActivity,
                                "" + loginResponse.message,
                                Toast.LENGTH_SHORT
                            ).show()
//                            mViewBinding.tvStatus.visibility = View.VISIBLE

                        }
                    }
                }
            })
    }

    fun getPointsApi(userid: String?) {
        mViewBinding.shimmerViewContainer.startShimmerAnimation()
        apiClient.getApiService().getPoints(userid)
            .enqueue(object : Callback<PointsResponse> {
                override fun onFailure(call: Call<PointsResponse>, t: Throwable) {
                    Toast.makeText(applicationContext, "false" + t.message, Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onResponse(
                    call: Call<PointsResponse>,
                    response: Response<PointsResponse>,
                ) {
                    val loginResponse = response.body()
                    if (loginResponse!!.status==true) {
                        if (!TextUtils.isEmpty(loginResponse.user.points)) {

                            Utilities.saveString(
                                this@MainActivity,
                                Utilities.USER_Points,
                                loginResponse.user.points.toString()
                            )
                            mViewBinding.tvPoints.setText(loginResponse.user.points)
                        }


                    } else {
//                        val loginResponse = response.body()

                        if (loginResponse != null) {
                            showToast(loginResponse.message)
                        }
                    }
                }
            })
    }

    override fun onPause() {
        mViewBinding.shimmerViewContainer.stopShimmerAnimation()
        super.onPause()
    }

    private fun showBottomDialog() {
         dialog = Dialog(this)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(false)
        dialog!!.setContentView(R.layout.welcome_sheet_dialog)
        val recyclerView = dialog!!.findViewById(R.id.recyclerview) as RecyclerView
        val rlCurrentLocation = dialog!!.findViewById(R.id.rlCurrentLocation) as RelativeLayout
        val rlAddAddress = dialog!!.findViewById(R.id.rlAddAddress) as RelativeLayout
        val shimmer_view_container = dialog!!.findViewById(R.id.shimmer_view_container) as ShimmerFrameLayout
        callApiForAddress(userid, recyclerView,shimmer_view_container)

        rlCurrentLocation.setOnClickListener {

            val intent = Intent(this, LocationActivity::class.java)
            startActivity(intent)
            dialog!!.dismiss()

        }
        rlAddAddress.setOnClickListener {

            val intent = Intent(this, AddAddressActivity::class.java)
            startActivity(intent)
            dialog!!.dismiss()

        }
        dialog!!.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.show()

    }

    fun callApiForAddress(
        userId: String?,
        recyclerview: RecyclerView,
        shimmerViewContainer: ShimmerFrameLayout,
    ) {
        shimmerViewContainer.startShimmerAnimation()

        apiClient.getApiService().getAaddress_list(userId)
            .enqueue(object : Callback<MainAddressResponse> {
                override fun onFailure(call: Call<MainAddressResponse>, t: Throwable) {
                    shimmerViewContainer.stopShimmerAnimation()
                    shimmerViewContainer.visibility = View.GONE
                    Toast.makeText(
                        this@MainActivity,
                        "failure case:   " + t.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()
//                    tvStatus.visibility = View.VISIBLE
                }

                override fun onResponse(
                    call: Call<MainAddressResponse>,
                    response: Response<MainAddressResponse>,
                ) {
                    val loginResponse = response.body()

                    if (loginResponse?.statusCode == 200) {
                        addressList = response.body()!!.getData
                        Toast.makeText(
                            this@MainActivity,
                            "" + loginResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()
                        shimmerViewContainer.stopShimmerAnimation()
                        shimmerViewContainer.visibility = View.GONE
                        recyclerview.visibility = View.VISIBLE
                        recyclerview.apply {
                            setHasFixedSize(true)
                            layoutManager =
                                GridLayoutManager(this@MainActivity,
                                    1,
                                    GridLayoutManager.VERTICAL,
                                    false)
//                            adapter = activity?.let { SuggestedAdapterNew(sugestedmodel, it) };
                            adapter = AddressAdapter(
                                this@MainActivity, addressList!!, this@MainActivity
                            )
                            mViewBinding.shimmerViewContainer.visibility = View.GONE


                        }
                    } else {
//                        val loginResponse = response.body()
                        if (loginResponse != null) {
                            Toast.makeText(
                                this@MainActivity,
                                "" + loginResponse.message,
                                Toast.LENGTH_SHORT
                            ).show()
                            shimmerViewContainer.stopShimmerAnimation()
                            shimmerViewContainer.visibility = View.GONE

                        }
                    }
                }
            })
    }

    override fun onItemClicked(pos: Int) {
        dialog!!.dismiss()
        Utilities.saveString(this,"savedAddress","yes")
        callApi(userCityName)
        getPointsApi(userid)
    }
}