package com.cubixsoft.mrmahircloneapp.models

import com.google.gson.annotations.SerializedName

data class PointsData(

    @SerializedName("points")
    val points: String? = null
)

