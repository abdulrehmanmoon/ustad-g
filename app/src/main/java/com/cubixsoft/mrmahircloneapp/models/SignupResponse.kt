package com.cubixsoft.mrmahircloneapp.models

import com.cubixsoft.mrmahircloneapp.models.SignupUser
import com.google.gson.annotations.SerializedName

data class SignupResponse (
    @SerializedName("status")
    var statusCode: Int,

    @SerializedName("message")
    var message: String,

    @SerializedName("data")
    var user: SignupUser
)