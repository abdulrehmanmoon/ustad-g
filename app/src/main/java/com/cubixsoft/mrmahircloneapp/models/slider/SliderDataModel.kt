package com.cubixsoft.mrmahircloneapp.models.slider

import com.google.gson.annotations.SerializedName

data class SliderDataModel(
    @SerializedName("id")
    var id: Int?,

    @SerializedName("name")
    var name: String,

    @SerializedName("city")
    var city: String,

    @SerializedName("category_id")
    var category_id: String,

    @SerializedName("image")
    val image: String,

    )