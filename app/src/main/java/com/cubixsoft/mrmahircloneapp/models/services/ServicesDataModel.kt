package com.myapps.healthapp1.models.subCategories

import com.google.gson.annotations.SerializedName

data class ServicesDataModel(
    @SerializedName("id")
    var id: Int,

    @SerializedName("name")
    var name: String,

    @SerializedName("image")
    val image: String,
    @SerializedName("price")
    val price: String,
    @SerializedName("city")
    val city: String,

    @SerializedName("orders")
    val orders: String,
    @SerializedName("rate")
    val rate: String,

    @SerializedName("price_type")
    val price_type: String,
    @SerializedName("sub_category_id")
    val sub_category_id: String,

    )