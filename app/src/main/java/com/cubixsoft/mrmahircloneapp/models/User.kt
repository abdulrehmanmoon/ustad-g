package com.cubixsoft.mrmahircloneapp.models

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("email")
    val email: String? = null,

    @SerializedName("phone")
    val phone: String? = null,

    @SerializedName("profile_image")
    val profile_image: String? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("otp")
    val otp : Int?=null,
)

