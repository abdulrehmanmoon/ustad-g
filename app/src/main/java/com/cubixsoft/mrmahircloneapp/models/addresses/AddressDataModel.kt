package com.cubixsoft.mrmahircloneapp.models.addresses

import com.google.gson.annotations.SerializedName

data class AddressDataModel(
    @SerializedName("id")
    var id: Int?,

    @SerializedName("user_id")
    var user_id: String,

    @SerializedName("title")
    val title: String,

    @SerializedName("string")
    var string: String,

    @SerializedName("lat")
    val lat: String,
    @SerializedName("lang")
    val lang: String,

    @SerializedName("created_at")
    var created_at: String,

    @SerializedName("updated_at")
    val updated_at: String,

    )