package com.cubixsoft.mrmahircloneapp.models.notifications

import com.cubixsoft.mrmahircloneapp.models.categories.CategoryDataModel
import com.cubixsoft.mrmahircloneapp.models.domain.DomainDataModel
import com.google.gson.annotations.SerializedName

data class MainNotificationResponse(
    @SerializedName("status")
    var statusCode: Boolean,

    @SerializedName("message")
    var message: String,

    @SerializedName("data")
    var getData: List<NotificationDataModel>,
)