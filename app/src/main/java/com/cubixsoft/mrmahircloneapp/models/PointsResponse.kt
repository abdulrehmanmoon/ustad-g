package com.cubixsoft.mrmahircloneapp.models

import com.google.gson.annotations.SerializedName

data class PointsResponse(
    @SerializedName("status")
    var status: Boolean,

    @SerializedName("message")
    var message: String,

    @SerializedName("data")
    var user: PointsData
)