package com.myapps.healthapp1.models.subCategories

import com.google.gson.annotations.SerializedName

data class OrderServicesDataModel(
    @SerializedName("id")
    var id: Int?,

    @SerializedName("number")
    var number: String,

    @SerializedName("service")
    val service: String,

    @SerializedName("amount")
    val amount: Int,

    )