package com.cubixsoft.mrmahircloneapp.models.slider

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

data class MainSliderResponse(
    @SerializedName("status")
    var statusCode: Boolean,

    @SerializedName("message")
    var message: String,

    @SerializedName("data")
    var getData: ArrayList<SliderDataModel>,
)