package com.myapps.healthapp1.models.subCategories

import com.google.gson.annotations.SerializedName

data class SubCategoryDataModel(
    @SerializedName("id")
    var id: Int?,

    @SerializedName("name")
    var name: String,

    @SerializedName("image")
    val image: String,
   @SerializedName("category_id")
    val category_id: Int,

    )