package com.cubixsoft.mrmahircloneapp.models

import com.google.gson.annotations.SerializedName

data class CommonResponse (
    @SerializedName("status")
    var statusCode: Boolean,

    @SerializedName("message")
    var message: String


)