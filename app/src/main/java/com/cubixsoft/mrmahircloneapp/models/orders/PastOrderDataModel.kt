package com.myapps.healthapp1.models.subCategories

import com.cubixsoft.mrmahircloneapp.models.OrderAddress
import com.google.gson.annotations.SerializedName

data class PastOrderDataModel(
    @SerializedName("id")
    var id: Int?,

    @SerializedName("status")
    var status: String,

    @SerializedName("date")
    val date: String,
    @SerializedName("time")
    val time: String,
    @SerializedName("ordernumber")
    val ordernumber: String,

    @SerializedName("scadualed")
    val scadualed: String,


    @SerializedName("sub_category")
    val sub_category: String,


    @SerializedName("total_amount")
    val total_amount: Int,

    @SerializedName("items")
    var getItems: List<OrderServicesDataModel>,

        @SerializedName("address")
    var getAddress: OrderAddress,


)