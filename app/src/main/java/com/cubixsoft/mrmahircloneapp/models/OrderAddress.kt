package com.cubixsoft.mrmahircloneapp.models

import com.google.gson.annotations.SerializedName

data class OrderAddress(
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("title")
    val title: String? = null,

    @SerializedName("city")
    val city: String? = null,

    @SerializedName("area")
    val area: String? = null,
    @SerializedName("street")
    val street: String? = null
)

