package com.cubixsoft.mrmahircloneapp.models.categories

import com.google.gson.annotations.SerializedName

data class CategoryDataModel(
    @SerializedName("id")
    var id: Int?,

    @SerializedName("name")
    var name: String,

    @SerializedName("image")
    val image: String,

    )