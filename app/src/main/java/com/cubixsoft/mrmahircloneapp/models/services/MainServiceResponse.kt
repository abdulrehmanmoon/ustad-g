package com.myapps.healthapp1.models.subCategories

import com.google.gson.annotations.SerializedName

data class MainServiceResponse(
    @SerializedName("status")
    var statusCode: Boolean,

    @SerializedName("message")
    var message: String,

    @SerializedName("data")
    var getData: List<ServicesDataModel>,
)