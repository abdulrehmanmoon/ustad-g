package com.cubixsoft.mrmahircloneapp.models.domain

import com.google.gson.annotations.SerializedName

data class DomainDataModel(
    @SerializedName("id")
    var id: Int?,

    @SerializedName("name")
    var name: String,

    @SerializedName("image")
    val image: String,

    @SerializedName("urdu_name")
    var urdu_name: String,

    @SerializedName("city")
    val city: String,
    @SerializedName("enable")
    val enable: String,

    @SerializedName("created_at")
    var created_at: String,

    @SerializedName("updated_at")
    val updated_at: String,

    )