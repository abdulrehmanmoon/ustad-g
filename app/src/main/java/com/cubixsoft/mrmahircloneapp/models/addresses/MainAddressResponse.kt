package com.cubixsoft.mrmahircloneapp.models.addresses

import com.cubixsoft.mrmahircloneapp.models.categories.CategoryDataModel
import com.cubixsoft.mrmahircloneapp.models.domain.DomainDataModel
import com.google.gson.annotations.SerializedName

data class MainAddressResponse(
    @SerializedName("status")
    var statusCode: Int,

    @SerializedName("message")
    var message: String,

    @SerializedName("data")
    var getData: List<AddressDataModel>,
)