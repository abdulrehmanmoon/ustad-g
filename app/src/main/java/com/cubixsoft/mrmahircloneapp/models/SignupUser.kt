package com.cubixsoft.mrmahircloneapp.models

import com.google.gson.annotations.SerializedName

data class SignupUser (
    @SerializedName("id")
    var id: String,

    @SerializedName("name")
    var name: String,

    @SerializedName("email")
    var email: String,

    @SerializedName("phone")
    var phone: String,

    @SerializedName("profile_image")
    var profile_image: String


)