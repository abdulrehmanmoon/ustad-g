package com.cubixsoft.mrmahircloneapp.models.services

import com.google.gson.annotations.SerializedName

data class SearchSuggestionModel(
    @SerializedName("name")
    var destination_name: String,
    )

