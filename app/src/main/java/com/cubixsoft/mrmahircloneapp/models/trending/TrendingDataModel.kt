package com.cubixsoft.mrmahircloneapp.models.trending

import com.google.gson.annotations.SerializedName

data class TrendingDataModel(
    @SerializedName("id")
    var id: Int?,

    @SerializedName("name")
    var name: String,

    @SerializedName("sub_category_id")
    var sub_category_id: String,

    @SerializedName("rate")
    var rate: String,

    @SerializedName("price_type")
    var price_type: String,
   @SerializedName("orders")
    var orders: String,

    @SerializedName("price")
    var price: String,

    @SerializedName("city")
    var city: String,

    @SerializedName("image")
    val image: String,

    )