package com.cubixsoft.mrmahircloneapp.models.checkoutModels

import com.google.gson.annotations.SerializedName

class CheckOutResponseModel(
    @field:SerializedName("status") var status: Boolean, @field:SerializedName(
        "message"
    ) var message: String
)
