package com.cubixsoft.mrmahircloneapp.models.trending

import com.google.gson.annotations.SerializedName

data class MainTrendingResponse(
    @SerializedName("status")
    var statusCode: Boolean,

    @SerializedName("message")
    var message: String,

    @SerializedName("data")
    var getData: List<TrendingDataModel>,
)