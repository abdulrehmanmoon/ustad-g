package com.cubixsoft.mrmahircloneapp.models

import com.google.gson.annotations.SerializedName

data class UserProfileImageResponse(
    @SerializedName("status")
    var status: Int,

    @SerializedName("message")
    var message: String,

    @SerializedName("data")
    var user: User
)