package com.cubixsoft.mrmahircloneapp.models.notifications

import com.google.gson.annotations.SerializedName

data class NotificationDataModel(
    @SerializedName("id")
    var id: Int?,

    @SerializedName("text")
    var text: String,

    @SerializedName("created_at")
    var created_at: String,

    @SerializedName("updated_at")
    var updated_at: String,

    )