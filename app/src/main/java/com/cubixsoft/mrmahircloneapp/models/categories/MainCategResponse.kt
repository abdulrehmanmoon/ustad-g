package com.cubixsoft.mrmahircloneapp.models.categories

import com.google.gson.annotations.SerializedName

data class MainCategResponse(
    @SerializedName("status")
    var statusCode: Boolean,

    @SerializedName("message")
    var message: String,

    @SerializedName("data")
    var getData: List<CategoryDataModel>,
)