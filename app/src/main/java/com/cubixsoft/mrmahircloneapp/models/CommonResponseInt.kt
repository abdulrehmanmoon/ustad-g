package com.cubixsoft.mrmahircloneapp.models

import com.google.gson.annotations.SerializedName

data class CommonResponseInt (
    @SerializedName("status")
    var statusCode: Int,

    @SerializedName("message")
    var message: String


)