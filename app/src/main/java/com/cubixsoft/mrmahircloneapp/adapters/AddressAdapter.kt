package com.cubixsoft.mrmahircloneapp.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.StrikethroughSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.cubixsoft.mrmahircloneapp.HomeActivity
import com.cubixsoft.mrmahircloneapp.R
import com.cubixsoft.mrmahircloneapp.Utilities
import com.cubixsoft.mrmahircloneapp.models.addresses.AddressDataModel
import com.cubixsoft.mrmahircloneapp.models.domain.DomainDataModel
import com.cubixsoft.mrmahircloneapp.services.Constants
import com.cubixsoft.mrmahircloneapp.utils.CartServicesClickListner
import com.myapps.healthapp1.models.subCategories.ServicesDataModel
import com.wang.avi.AVLoadingIndicatorView
import java.util.*

class AddressAdapter(
    var context: Context,
    var raceModel: List<AddressDataModel>,
    var callback: Callback
) :
    RecyclerView.Adapter<AddressAdapter.ViewHolder>() {
    override fun onCreateViewHolder(container: ViewGroup, i: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.row_address,
                container,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, i: Int) = holder.bind(i)

    override fun getItemCount(): Int = raceModel.size

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvTitle = itemView.findViewById(R.id.tvTitle) as TextView
        var tvAddress = itemView.findViewById(R.id.tvAddress) as TextView
        var ivImage = itemView.findViewById(R.id.ivImage) as ImageView


        fun bind(pos: Int) {
            val model = raceModel.get(pos)
            tvTitle.text = model.title
            tvAddress.text = model.string

            initClickListeners()
        }

        private fun initClickListeners() {
            itemView.setOnClickListener {
                callback.onItemClicked(adapterPosition)

            }
        }
    }


    interface Callback {
        fun onItemClicked(pos: Int)
    }
    init {
        this.raceModel = raceModel
        this.context = context

    }
}
