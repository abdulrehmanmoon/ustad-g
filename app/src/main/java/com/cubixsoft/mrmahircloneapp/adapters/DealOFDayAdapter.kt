package com.cubixsoft.mrmahircloneapp.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.cubixsoft.mrmahircloneapp.R
import com.cubixsoft.mrmahircloneapp.Utilities
import com.cubixsoft.mrmahircloneapp.activities.PlaceOrderActivity
import com.cubixsoft.mrmahircloneapp.models.trending.TrendingDataModel
import com.cubixsoft.mrmahircloneapp.services.Constants
import com.wang.avi.AVLoadingIndicatorView
import java.util.*

class DealOFDayAdapter(
    private val context: Context,
    private val raceModel: List<TrendingDataModel>?,
) :
    RecyclerView.Adapter<DealOFDayAdapter.ViewHolder>() {


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.tv_name_product)
        val tvprice: TextView = itemView.findViewById(R.id.tvprice)
        val totalOrder: TextView = itemView.findViewById(R.id.totalOrder)
        val image: ImageView = itemView.findViewById(R.id.imageView)

        //        val imageTrans: ImageView = itemView.findViewById(R.id.ima)
        val imageLoading: AVLoadingIndicatorView = itemView.findViewById(R.id.imageLoading)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.row_deal_of_day, parent, false)
        return ViewHolder(itemView)
    }

    @SuppressLint("SimpleDateFormat", "SetTextI18n", "ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        //
        val model = raceModel?.get(position)
        if (model != null) {
//            if(position%2==0){
//                holder.imageTrans.setBackgroundColor(context.getResources().getColor(R.color.transparent_green));
//
//            } else if(position%3==0){
//                holder.imageTrans.setBackgroundColor(context.getResources().getColor(R.color.transparent_blue));
//
//            } else {
//                holder.imageTrans.setBackgroundColor(context.getResources().getColor(R.color.transparent_red));
//
//            }

            holder.title.text = model.name
            holder.tvprice.text = "Rs: " + model.price
            holder.totalOrder.text = model.orders + " orders"
            val imageurl: String = model.image
            val search = "http"

            if (!TextUtils.isEmpty(imageurl)) {
                holder.imageLoading.setVisibility(View.VISIBLE)
                if (imageurl.lowercase(Locale.getDefault()).trim()
                        .contains(search.lowercase(Locale.getDefault()).trim())
                ) {
                    Glide.with(context).load(Constants.BASE_URL_IMG +imageurl)
                        .listener(object : RequestListener<Drawable?> {
                            override fun onLoadFailed(
                                e: GlideException?,
                                model: Any,
                                target: Target<Drawable?>,
                                isFirstResource: Boolean,
                            ): Boolean {
                                holder.imageLoading.setVisibility(View.GONE)
                                return false
                            }

                            override fun onResourceReady(
                                resource: Drawable?,
                                model: Any,
                                target: Target<Drawable?>,
                                dataSource: DataSource,
                                isFirstResource: Boolean,
                            ): Boolean {
                                holder.imageLoading.setVisibility(View.GONE)
                                return false
                            }
                        }).into(holder.image)
                } else {
                    Glide.with(context).load(Constants.BASE_URL_IMG + imageurl)
                        .listener(object : RequestListener<Drawable?> {
                            override fun onLoadFailed(
                                e: GlideException?,
                                model: Any,
                                target: Target<Drawable?>,
                                isFirstResource: Boolean,
                            ): Boolean {
                                holder.imageLoading.setVisibility(View.GONE)
                                return false
                            }

                            override fun onResourceReady(
                                resource: Drawable?,
                                model: Any,
                                target: Target<Drawable?>,
                                dataSource: DataSource,
                                isFirstResource: Boolean,
                            ): Boolean {
                                holder.imageLoading.setVisibility(View.GONE)
                                return false
                            }
                        }).into(holder.image)
//                Picasso.get().load(memberListModels.get(position).getImage()).placeholder(R.drawable.placeholder).into(holder1.ivImage);
                }
            }
//             sets the image to the imageview from our itemHolder class
//            Glide.with(context).load(model.image).into(holder.image)

        }




        holder.itemView.setOnClickListener {
            Utilities.saveString(context, "categoryId", model!!.id.toString())
            Utilities.saveString(context, "title", model.name.toString())
//            Utilities.saveInt(context, "position1", position)
//            Utilities.setPastBookingModels(context,raceModel)
//            Utilities.setPickUpPoints(context, model?.pickupPointsModel)
//            (context as MainActivity).navController.navigate(R.id.action_homeFragment_to_categoryFragment)
//            Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_categoryFragment)
            context.startActivity(Intent(context, PlaceOrderActivity::class.java))
        }

    }


    override fun getItemCount() = raceModel!!.size

}
