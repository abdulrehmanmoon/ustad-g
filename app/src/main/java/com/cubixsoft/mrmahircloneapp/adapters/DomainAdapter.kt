package com.cubixsoft.mrmahircloneapp.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.cubixsoft.mrmahircloneapp.HomeActivity
import com.cubixsoft.mrmahircloneapp.R
import com.cubixsoft.mrmahircloneapp.Utilities
import com.cubixsoft.mrmahircloneapp.models.domain.DomainDataModel
import com.cubixsoft.mrmahircloneapp.services.Constants
import com.wang.avi.AVLoadingIndicatorView
import java.util.*

class DomainAdapter(private val context: Context, private val raceModel: List<DomainDataModel>?) :
    RecyclerView.Adapter<DomainAdapter.ViewHolder>() {


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.tvTitle)
        val tvDescription: TextView = itemView.findViewById(R.id.tvDescription)
        val image: ImageView = itemView.findViewById(R.id.ivImage)
        val imageLoading: AVLoadingIndicatorView = itemView.findViewById(R.id.imageLoading)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.row_domain, parent, false)
        return ViewHolder(itemView)
    }

    @SuppressLint("SimpleDateFormat", "SetTextI18n", "ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        //
        val model = raceModel?.get(position)
        if (model != null) {


            holder.title.text = model.name
            holder.tvDescription.text = model.urdu_name
            val imageurl: String = model.image
            val search = "http"

            if (!TextUtils.isEmpty(imageurl)) {
                holder.imageLoading.setVisibility(View.VISIBLE)
                if (imageurl.lowercase(Locale.getDefault()).trim()
                        .contains(search.lowercase(Locale.getDefault()).trim())
                ) {
                    Glide.with(context).load(Constants.BASE_URL_IMG + imageurl)
                        .listener(object : RequestListener<Drawable?> {
                            override fun onLoadFailed(
                                e: GlideException?,
                                model: Any,
                                target: Target<Drawable?>,
                                isFirstResource: Boolean,
                            ): Boolean {
                                holder.imageLoading.setVisibility(View.GONE)
                                return false
                            }

                            override fun onResourceReady(
                                resource: Drawable?,
                                model: Any,
                                target: Target<Drawable?>,
                                dataSource: DataSource,
                                isFirstResource: Boolean,
                            ): Boolean {
                                holder.imageLoading.setVisibility(View.GONE)
                                return false
                            }
                        }).into(holder.image)
                } else {
                    Glide.with(context).load(Constants.BASE_URL_IMG + imageurl)
                        .listener(object : RequestListener<Drawable?> {
                            override fun onLoadFailed(
                                e: GlideException?,
                                model: Any,
                                target: Target<Drawable?>,
                                isFirstResource: Boolean,
                            ): Boolean {
                                holder.imageLoading.setVisibility(View.GONE)
                                return false
                            }

                            override fun onResourceReady(
                                resource: Drawable?,
                                model: Any,
                                target: Target<Drawable?>,
                                dataSource: DataSource,
                                isFirstResource: Boolean,
                            ): Boolean {
                                holder.imageLoading.setVisibility(View.GONE)
                                return false
                            }
                        }).into(holder.image)
//                Picasso.get().load(memberListModels.get(position).getImage()).placeholder(R.drawable.placeholder).into(holder1.ivImage);
                }
            }
        }




        holder.itemView.setOnClickListener {
            Utilities.saveString(context, "domainId", model!!.id.toString())
//            Utilities.saveString(context, "title", model.name.toString())

            context.startActivity(Intent(context, HomeActivity::class.java))
//            Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_categoriesFragment)

        }

    }


    override fun getItemCount() = raceModel!!.size

}
