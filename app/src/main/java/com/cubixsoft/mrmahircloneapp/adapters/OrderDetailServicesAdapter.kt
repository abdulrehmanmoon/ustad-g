package com.cubixsoft.mrmahircloneapp.adapters

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.StrikethroughSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.cubixsoft.mrmahircloneapp.R
import com.cubixsoft.mrmahircloneapp.services.Constants
import com.myapps.healthapp1.models.subCategories.OrderServicesDataModel
import com.wang.avi.AVLoadingIndicatorView
import java.util.*


class OrderDetailServicesAdapter(
    var context: Context,
    var raceModel: List<OrderServicesDataModel>
) :
    RecyclerView.Adapter<OrderDetailServicesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(container: ViewGroup, i: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.row_services,
                container,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, i: Int) = holder.bind(i)

    override fun getItemCount(): Int = raceModel.size

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var layout_item = itemView.findViewById(R.id.cvMain) as RelativeLayout
        var llTop = itemView.findViewById(R.id.llTop) as CardView
        var title = itemView.findViewById(R.id.textView) as TextView
        var price = itemView.findViewById(R.id.tvnewPrice) as TextView
        var priceType = itemView.findViewById(R.id.priceType) as TextView
        var imageLoading = itemView.findViewById(R.id.imageLoading) as AVLoadingIndicatorView
        var oldprice = itemView.findViewById(R.id.oldprice) as TextView
        var image = itemView.findViewById(R.id.profile_image) as ImageView


        fun bind(pos: Int) {
//            tvChildName.text = childrenList[pos].fname + " " + childrenList[pos].lname
//            picktime.text = childrenList[pos].pickup_time
//            tvDropTime.text = childrenList[pos].drop_time
//            tvpickuplocation.text = childrenList[pos].pickup_location
//            institute_name.text = childrenList[pos].institute_name
//            tvdroplocation.text = childrenList[pos].drop_location

            val model = raceModel.get(pos)
            title.text = model.service
            price.text = "Rs: " + model.amount
//            priceType.text = model.price_type
//            val imageurl: String = model.image
            val content1 = "Rs: 100"
            val spannableString1 = SpannableString(content1)
            spannableString1.setSpan(StrikethroughSpan(), 0, content1.length, 0)
            oldprice.text = spannableString1
//            val imageurl: String = model.im
//            val search = "http"
//            if (!TextUtils.isEmpty(imageurl)) {
//                holder.imageLoading.setVisibility(View.VISIBLE)
//                if (imageurl.lowercase(Locale.getDefault()).trim()
//                        .contains(search.lowercase(Locale.getDefault()).trim())
//                ) {
//                    Glide.with(context).load(Constants.BASE_URL_IMG +imageurl)
//                        .listener(object : RequestListener<Drawable?> {
//                            override fun onLoadFailed(
//                                e: GlideException?,
//                                model: Any,
//                                target: Target<Drawable?>,
//                                isFirstResource: Boolean,
//                            ): Boolean {
//                                holder.imageLoading.setVisibility(View.GONE)
//                                return false
//                            }
//
//                            override fun onResourceReady(
//                                resource: Drawable?,
//                                model: Any,
//                                target: Target<Drawable?>,
//                                dataSource: DataSource,
//                                isFirstResource: Boolean,
//                            ): Boolean {
//                                holder.imageLoading.setVisibility(View.GONE)
//                                return false
//                            }
//                        }).into(holder.image)
//                } else {
//                    Glide.with(context).load(Constants.BASE_URL_IMG + imageurl)
//                        .listener(object : RequestListener<Drawable?> {
//                            override fun onLoadFailed(
//                                e: GlideException?,
//                                model: Any,
//                                target: Target<Drawable?>,
//                                isFirstResource: Boolean,
//                            ): Boolean {
//                                holder.imageLoading.setVisibility(View.GONE)
//                                return false
//                            }
//
//                            override fun onResourceReady(
//                                resource: Drawable?,
//                                model: Any,
//                                target: Target<Drawable?>,
//                                dataSource: DataSource,
//                                isFirstResource: Boolean,
//                            ): Boolean {
//                                holder.imageLoading.setVisibility(View.GONE)
//                                return false
//                            }
//                        }).into(holder.image)
////                Picasso.get().load(memberListModels.get(position).getImage()).placeholder(R.drawable.placeholder).into(holder1.ivImage);
//                }
//            }

//            Picasso.get().load(Constants.BASE_URL_IMG + imageurl).into(image)
            // sets the image to the imageview from our itemHolder class

//            initClickListeners()
        }

//        private fun initClickListeners() {
//            layout_item.setOnClickListener {
//                callback.onItemClicked(adapterPosition)
//
//            }
//        }
    }

    interface Callback {
        fun onItemClicked(pos: Int)
//        fun onDeleteClicked(pos: Int)
//        fun oncvItemClicked(pos: Int)
    }
}