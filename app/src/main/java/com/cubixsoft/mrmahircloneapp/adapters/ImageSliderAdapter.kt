package com.cubixsoft.mrmahircloneapp.adapters

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.cubixsoft.mrmahircloneapp.R
import com.cubixsoft.mrmahircloneapp.models.slider.SliderDataModel
import com.cubixsoft.mrmahircloneapp.services.Constants
import com.makeramen.roundedimageview.RoundedImageView
import com.wang.avi.AVLoadingIndicatorView
import libs.mjn.prettydialog.BuildConfig
import java.util.*

class ImageSliderAdapter(
    private val context: Context,
    list: ArrayList<SliderDataModel>,
    viewPager2: ViewPager2
) :
    RecyclerView.Adapter<ImageSliderAdapter.ViewHolder>() {
    private val list: ArrayList<SliderDataModel>
    var viewPager2: ViewPager2
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v: View = LayoutInflater.from(context)
            .inflate(R.layout.design_image_slider_container, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model: SliderDataModel = list[position]
        val imageurl: String = model.image.toString()
        val search = "http"
        if (!TextUtils.isEmpty(imageurl)) {
            holder.imageLoading.setVisibility(View.VISIBLE)
            if (imageurl.lowercase(Locale.getDefault()).trim()
                    .contains(search.lowercase(Locale.getDefault()).trim())
            ) {
                Glide.with(context).load(imageurl)
                    .listener(object : RequestListener<Drawable?> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any,
                            target: Target<Drawable?>,
                            isFirstResource: Boolean,
                        ): Boolean {
                            holder.imageLoading.setVisibility(View.GONE)
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any,
                            target: Target<Drawable?>,
                            dataSource: DataSource,
                            isFirstResource: Boolean,
                        ): Boolean {
                            holder.imageLoading.setVisibility(View.GONE)
                            return false
                        }
                    }).into(holder.image)
            } else {
                Glide.with(context).load(Constants.BASE_URL_IMG + imageurl)
                    .listener(object : RequestListener<Drawable?> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any,
                            target: Target<Drawable?>,
                            isFirstResource: Boolean,
                        ): Boolean {
                            holder.imageLoading.setVisibility(View.GONE)
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any,
                            target: Target<Drawable?>,
                            dataSource: DataSource,
                            isFirstResource: Boolean,
                        ): Boolean {
                            holder.imageLoading.setVisibility(View.GONE)
                            return false
                        }
                    }).into(holder.image)
//                Picasso.get().load(memberListModels.get(position).getImage()).placeholder(R.drawable.placeholder).into(holder1.ivImage);
            }
        }
        if (position == list.size - 2) {
            viewPager2.post(runnable)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image: RoundedImageView
        val  llShare:LinearLayout
        var imageLoading: AVLoadingIndicatorView

        init {
            image = itemView.findViewById(R.id.slider_image)
            llShare = itemView.findViewById(R.id.llShare)
            imageLoading = itemView.findViewById(R.id.imageLoading)
        }
    }

    private val runnable = Runnable {
        list.addAll(list)
        notifyDataSetChanged()
    }

    init {
        this.list = list
        this.viewPager2 = viewPager2
    }
}