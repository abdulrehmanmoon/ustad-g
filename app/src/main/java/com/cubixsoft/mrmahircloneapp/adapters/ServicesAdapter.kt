package com.cubixsoft.mrmahircloneapp.adapters

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.StrikethroughSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.cubixsoft.mrmahircloneapp.R
import com.cubixsoft.mrmahircloneapp.roomDbs.MyDatabase
import com.cubixsoft.mrmahircloneapp.services.Constants
import com.cubixsoft.mrmahircloneapp.utils.CartClickListner
import com.cubixsoft.mrmahircloneapp.utils.CartServicesClickListner
import com.myapps.healthapp1.models.subCategories.ServicesDataModel
import com.squareup.picasso.Picasso
import com.wang.avi.AVLoadingIndicatorView
import java.util.*


class ServicesAdapter(
    var context: Context,
    var raceModel: List<ServicesDataModel>,
    var callback: Callback,
    listener: CartServicesClickListner
) :
    RecyclerView.Adapter<ServicesAdapter.ViewHolder>() {
    private var selectedItemPosition: Int = 0
    var number = 0
     var cartClickListner: CartServicesClickListner?=null
    override fun onCreateViewHolder(container: ViewGroup, i: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.row_services,
                container,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, i: Int) = holder.bind(i)

    override fun getItemCount(): Int = raceModel.size

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var layout_item = itemView.findViewById(R.id.cvMain) as RelativeLayout
        var llTop = itemView.findViewById(R.id.llTop) as CardView
        var title = itemView.findViewById(R.id.textView) as TextView
        var price = itemView.findViewById(R.id.tvnewPrice) as TextView
        var priceType = itemView.findViewById(R.id.priceType) as TextView
        var tv_quantity = itemView.findViewById(R.id.tv_q_cart) as TextView
        var tv_Price = itemView.findViewById(R.id.tv_productPrice) as TextView

        var imageLoading = itemView.findViewById(R.id.imageLoading) as AVLoadingIndicatorView
        var oldprice = itemView.findViewById(R.id.oldprice) as TextView
        var image = itemView.findViewById(R.id.profile_image) as ImageView
        var btn_add = itemView.findViewById(R.id.img_max) as ImageView
        var btn_minus = itemView.findViewById(R.id.img_min) as ImageView


        fun bind(pos: Int) {
//            tvChildName.text = childrenList[pos].fname + " " + childrenList[pos].lname
//            picktime.text = childrenList[pos].pickup_time
//            tvDropTime.text = childrenList[pos].drop_time
//            tvpickuplocation.text = childrenList[pos].pickup_location
//            institute_name.text = childrenList[pos].institute_name
//            tvdroplocation.text = childrenList[pos].drop_location

            val model = raceModel.get(pos)
            title.text = model.name
            price.text = "Rs: "+model.price
            priceType.text = model.price_type
            val content1 = "Rs: 100"
            val spannableString1 = SpannableString(content1)
            spannableString1.setSpan(StrikethroughSpan(),0,content1.length,0)
            oldprice.text = spannableString1
            val imageurl: String = model.image
            val search = "http"
//            itemView.setOnClickListener {
//                selectedItemPosition = position
//                notifyDataSetChanged()
//            }
//            if(selectedItemPosition == position)
//                llTop.setBackgroundColor(Color.parseColor("#DC746C"))
//            else
//                llTop.setBackgroundColor(Color.parseColor("#E49B83"))
           btn_minus.setOnClickListener {
                number = tv_quantity.text.toString().toInt()
                if (number <= 1) {
                    number = 1
                } else {
                    number--
                    tv_quantity.text = "" + number
                }
                val price: String = model.price
                val value1 = if (price.isEmpty()) 0.0 else price.toDouble()
                val quantity = tv_quantity.text.toString()
                val value2 = if (quantity.isEmpty()) 0.0 else quantity.toDouble()
                val a: Double
                a = value1 * value2
                val finalValue = java.lang.Double.toString(a)
               tv_Price.text = finalValue
//                myAppDatabase.cartDao().update(quantity, raceModel[position].id)
               cartClickListner?.onMinusClick(model,pos)
            }
           btn_add.setOnClickListener {
                number =tv_quantity.text.toString().toInt()
                number++
                tv_quantity.text = "" + number
                val price: String = model.price
                val value1 = if (price.isEmpty()) 0.0 else price.toDouble()
                val quantity = tv_quantity.text.toString()
                val value2 = if (quantity.isEmpty()) 0.0 else quantity.toDouble()
                val a: Double
                a = value1 * value2
                val finalValue = java.lang.Double.toString(a)
               tv_Price.text = finalValue
//                myAppDatabase.cartDao().update(quantity, raceModel[position].id)
               cartClickListner?.onPlusClick(model,pos)
            }
            if (!TextUtils.isEmpty(imageurl)) {
                imageLoading.setVisibility(View.VISIBLE)
                if (imageurl.lowercase(Locale.getDefault()).trim()
                        .contains(search.lowercase(Locale.getDefault()).trim())
                ) {
                    Glide.with(context).load(Constants.BASE_URL_IMG +imageurl)
                        .listener(object : RequestListener<Drawable?> {
                            override fun onLoadFailed(
                                e: GlideException?,
                                model: Any,
                                target: Target<Drawable?>,
                                isFirstResource: Boolean,
                            ): Boolean {
                              imageLoading.setVisibility(View.GONE)
                                return false
                            }

                            override fun onResourceReady(
                                resource: Drawable?,
                                model: Any,
                                target: Target<Drawable?>,
                                dataSource: DataSource,
                                isFirstResource: Boolean,
                            ): Boolean {
                              imageLoading.setVisibility(View.GONE)
                                return false
                            }
                        }).into(image)
                } else {
                    Glide.with(context).load(Constants.BASE_URL_IMG + imageurl)
                        .listener(object : RequestListener<Drawable?> {
                            override fun onLoadFailed(
                                e: GlideException?,
                                model: Any,
                                target: Target<Drawable?>,
                                isFirstResource: Boolean,
                            ): Boolean {
                              imageLoading.setVisibility(View.GONE)
                                return false
                            }

                            override fun onResourceReady(
                                resource: Drawable?,
                                model: Any,
                                target: Target<Drawable?>,
                                dataSource: DataSource,
                                isFirstResource: Boolean,
                            ): Boolean {
                              imageLoading.setVisibility(View.GONE)
                                return false
                            }
                        }).into(image)
//                Picasso.get().load(memberListModels.get(position).getImage()).placeholder(R.drawable.placeholder).into(holder1.ivImage);
                }
            }
            // sets the image to the imageview from our itemHolder class

            initClickListeners()
        }

        private fun initClickListeners() {
            layout_item.setOnClickListener {
                callback.onItemClicked(adapterPosition)

            }
        }
    }


    interface Callback {
        fun onItemClicked(pos: Int)
//        fun onDeleteClicked(pos: Int)
//        fun oncvItemClicked(pos: Int)
    }
    init {
        this.raceModel = raceModel
        this.context = context
        cartClickListner = listener
//        myAppDatabase = Room.databaseBuilder(context, MyDatabase::class.java, "My_Cart")
//            .allowMainThreadQueries().build()
    }
}