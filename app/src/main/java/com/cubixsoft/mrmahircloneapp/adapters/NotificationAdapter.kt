package com.cubixsoft.mrmahircloneapp.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.cubixsoft.mrmahircloneapp.HomeActivity
import com.cubixsoft.mrmahircloneapp.R
import com.cubixsoft.mrmahircloneapp.Utilities
import com.cubixsoft.mrmahircloneapp.models.domain.DomainDataModel
import com.cubixsoft.mrmahircloneapp.models.notifications.NotificationDataModel
import com.cubixsoft.mrmahircloneapp.services.Constants
import com.wang.avi.AVLoadingIndicatorView
import java.util.*

class NotificationAdapter(private val context: Context, private val raceModel: List<NotificationDataModel>?) :
    RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.tvTitle)
        val tvTime: TextView = itemView.findViewById(R.id.tvTime)
        val image: ImageView = itemView.findViewById(R.id.ivImage)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.row_notifications, parent, false)
        return ViewHolder(itemView)
    }

    @SuppressLint("SimpleDateFormat", "SetTextI18n", "ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        //
        val model = raceModel?.get(position)
        if (model != null) {


            holder.title.text = model.text
            holder.tvTime.text = model.created_at
        }




        holder.itemView.setOnClickListener {
            Utilities.saveString(context, "domainId", model!!.id.toString())
//            Utilities.saveString(context, "title", model.name.toString())
//            Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_categoriesFragment)

        }

    }


    override fun getItemCount() = raceModel!!.size

}
