package com.cubixsoft.mrmahircloneapp.adapters
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.cubixsoft.mrmahircloneapp.HomeActivity
import com.cubixsoft.mrmahircloneapp.R
import com.cubixsoft.mrmahircloneapp.Utilities
import com.cubixsoft.mrmahircloneapp.services.Constants
import com.myapps.healthapp1.models.subCategories.SubCategoryDataModel
import com.wang.avi.AVLoadingIndicatorView
import java.util.*

class SubCategoryAdapter(private val context: Context, private val raceModel: List<SubCategoryDataModel>?) :
    RecyclerView.Adapter<SubCategoryAdapter.ViewHolder>() {
    var mColors = arrayOf("#3F51B5", "#FF9800", "#009688", "#673AB7")

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.textView)
        val image: ImageView = itemView.findViewById(R.id.profile_image)
        val llmain: LinearLayout = itemView.findViewById(R.id.llmain)
        val imageLoading: AVLoadingIndicatorView = itemView.findViewById(R.id.imageLoading)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.row_category, parent, false)
        return ViewHolder(itemView)
    }

    @SuppressLint("SimpleDateFormat", "SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        //
        val model = raceModel?.get(position)
        if (model != null) {
            holder.title.text = model.name
            val imageurl: String = model.image
            val search = "http"
//            holder.llmain.setBackgroundColor(Color.parseColor(mColors[position % 4])); // 4 can be replaced by mColors.length

            if (!TextUtils.isEmpty(imageurl)) {
                holder.imageLoading.setVisibility(View.VISIBLE)
                if (imageurl.lowercase(Locale.getDefault()).trim()
                        .contains(search.lowercase(Locale.getDefault()).trim())
                ) {
                    Glide.with(context).load(Constants.BASE_URL_IMG + imageurl)
                        .listener(object : RequestListener<Drawable?> {
                            override fun onLoadFailed(
                                e: GlideException?,
                                model: Any,
                                target: Target<Drawable?>,
                                isFirstResource: Boolean,
                            ): Boolean {
                                holder.imageLoading.setVisibility(View.GONE)
                                return false
                            }

                            override fun onResourceReady(
                                resource: Drawable?,
                                model: Any,
                                target: Target<Drawable?>,
                                dataSource: DataSource,
                                isFirstResource: Boolean,
                            ): Boolean {
                                holder.imageLoading.setVisibility(View.GONE)
                                return false
                            }
                        }).into(holder.image)
                } else {
                    Glide.with(context).load(Constants.BASE_URL_IMG + imageurl)
                        .listener(object : RequestListener<Drawable?> {
                            override fun onLoadFailed(
                                e: GlideException?,
                                model: Any,
                                target: Target<Drawable?>,
                                isFirstResource: Boolean,
                            ): Boolean {
                                holder.imageLoading.setVisibility(View.GONE)
                                return false
                            }

                            override fun onResourceReady(
                                resource: Drawable?,
                                model: Any,
                                target: Target<Drawable?>,
                                dataSource: DataSource,
                                isFirstResource: Boolean,
                            ): Boolean {
                                holder.imageLoading.setVisibility(View.GONE)
                                return false
                            }
                        }).into(holder.image)
////                Picasso.get().load(memberListModels.get(position).getImage()).placeholder(R.drawable.placeholder).into(holder1.ivImage);
//                }}
                    // sets the image to the imageview from our itemHolder class

                }




                holder.itemView.setOnClickListener {
                    Utilities.saveString(context, "subCategoryId", model?.id.toString())
//            Utilities.saveString(context,"title", model?.name.toString())
                    Utilities.saveString(context, "title", holder.title.text.toString())

//            Utilities.saveInt(context, "position1", position)
//            Utilities.setPastBookingModels(context,raceModel)
//            Utilities.setPickUpPoints(context, model?.pickupPointsModel)
                    (context as HomeActivity).navController.navigate(R.id.action_subCategoryFragment_to_servicesFragment)
//            Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_categoryFragment)

                }

            }
        }
    }

    override fun getItemCount() = raceModel!!.size

}
