package com.cubixsoft.mrmahircloneapp.adapters
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.cubixsoft.mehrancashcarry.models.ItemsViewModel
import com.cubixsoft.mrmahircloneapp.HomeActivity
import com.cubixsoft.mrmahircloneapp.MainActivity
import com.cubixsoft.mrmahircloneapp.R
import com.cubixsoft.mrmahircloneapp.Utilities
import com.cubixsoft.mrmahircloneapp.models.categories.CategoryDataModel
import com.cubixsoft.mrmahircloneapp.services.Constants
import com.wang.avi.AVLoadingIndicatorView
import java.util.*

class CategoryAdapter(private val context: Context, private val raceModel: List<CategoryDataModel>?) :
    RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.textView)
        val image: ImageView = itemView.findViewById(R.id.imageView)
        val imageLoading: AVLoadingIndicatorView = itemView.findViewById(R.id.imageLoading)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.single_cat_layout_new, parent, false)
        return ViewHolder(itemView)
    }

    @SuppressLint("SimpleDateFormat", "SetTextI18n", "ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        //
        val model = raceModel?.get(position)
        if (model != null) {


            holder.title.text = model.name
            val imageurl: String = model.image
            val search = "http"

            if (!TextUtils.isEmpty(imageurl)) {
                holder.imageLoading.setVisibility(View.VISIBLE)
                if (imageurl.lowercase(Locale.getDefault()).trim()
                        .contains(search.lowercase(Locale.getDefault()).trim())
                ) {
                    Glide.with(context).load(Constants.BASE_URL_IMG +imageurl)
                        .listener(object : RequestListener<Drawable?> {
                            override fun onLoadFailed(
                                e: GlideException?,
                                model: Any,
                                target: Target<Drawable?>,
                                isFirstResource: Boolean,
                            ): Boolean {
                                holder.imageLoading.setVisibility(View.GONE)
                                return false
                            }

                            override fun onResourceReady(
                                resource: Drawable?,
                                model: Any,
                                target: Target<Drawable?>,
                                dataSource: DataSource,
                                isFirstResource: Boolean,
                            ): Boolean {
                                holder.imageLoading.setVisibility(View.GONE)
                                return false
                            }
                        }).into(holder.image)
                } else {
                    Glide.with(context).load(Constants.BASE_URL_IMG + imageurl)
                        .listener(object : RequestListener<Drawable?> {
                            override fun onLoadFailed(
                                e: GlideException?,
                                model: Any,
                                target: Target<Drawable?>,
                                isFirstResource: Boolean,
                            ): Boolean {
                                holder.imageLoading.setVisibility(View.GONE)
                                return false
                            }

                            override fun onResourceReady(
                                resource: Drawable?,
                                model: Any,
                                target: Target<Drawable?>,
                                dataSource: DataSource,
                                isFirstResource: Boolean,
                            ): Boolean {
                                holder.imageLoading.setVisibility(View.GONE)
                                return false
                            }
                        }).into(holder.image)
//                Picasso.get().load(memberListModels.get(position).getImage()).placeholder(R.drawable.placeholder).into(holder1.ivImage);
                }
            }
        }




        holder.itemView.setOnClickListener {
            Utilities.saveString(context,"categoryId", model!!.id.toString())
            Utilities.saveString(context,"title", model.name.toString())

            (context as HomeActivity).navController.navigate(R.id.action_homeFragment_to_subCategoryFragment)
//            Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_categoriesFragment)

        }

    }


    override fun getItemCount() = raceModel!!.size

}
