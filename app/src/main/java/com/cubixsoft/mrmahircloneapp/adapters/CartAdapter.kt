package com.cubixsoft.mrmahircloneapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.cubixsoft.mrmahircloneapp.R
import com.cubixsoft.mrmahircloneapp.Utilities
import com.cubixsoft.mrmahircloneapp.roomDbs.Cart
import com.cubixsoft.mrmahircloneapp.roomDbs.MyDatabase
import com.cubixsoft.mrmahircloneapp.utils.CartClickListner
import kotlinx.coroutines.NonDisposableHandle.parent
import libs.mjn.prettydialog.PrettyDialog

class CartAdapter(carts: List<Cart>, context: Context, listener: CartClickListner) : RecyclerView.Adapter<CartAdapter.ViewHolder>() {
    var carts: List<Cart>
    var context: Context
    private val myAppDatabase: MyDatabase
    var number = 0
    private val cartClickListner: CartClickListner
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.iv_add_to_cart_product_buyer, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val cart: Cart = carts[position]
        val prodcut_currency: String = Utilities.getString(context, "prodcut_currency").toString()
        val price: Float = cart.price.toFloat()
        val qty: Float = cart.quantity.toFloat()
        val product = price * qty
        holder.tv_Price.text = prodcut_currency + product.toString()
        holder.tv_Name.setText(cart.name)
        holder.tv_productdesc.setText(cart.desc)
        holder.tv_quantity.setText(cart.quantity)
        holder.ivCross.setOnClickListener {
            Toast.makeText(context, "Successfully Deleted", Toast.LENGTH_SHORT).show()
            val database: MyDatabase =
                Room.databaseBuilder(context, MyDatabase::class.java, "My_Cart")
                    .allowMainThreadQueries().build()
            val id: Int = carts[position].id
            database.cartDao().deleteItem(id)
//            carts.removeAt(position)
            notifyDataSetChanged()
            cartClickListner.onDeleteClick(cart)
//            val builder = AlertDialog.Builder(context)
//            builder.setTitle(R.string.app_name)
//            builder.setMessage("Are you sure you want to Delete This Item?")
//            builder.setPositiveButton("Confirm") { dialogInterface: DialogInterface, i: Int ->
//                Toast.makeText(context, "Successfully Deleted", Toast.LENGTH_SHORT).show()
//                val database: MyDatabase =
//                    Room.databaseBuilder(context, MyDatabase::class.java, "My_Cart")
//                        .allowMainThreadQueries().build()
//                val id: Int = carts[position].id
//                database.cartDao().deleteItem(id)
////            carts.removeAt(position)
//                notifyDataSetChanged()
//                cartClickListner.onDeleteClick(cart)
//                dialogInterface.dismiss()
//            }
//            builder.setNegativeButton("Cancel") { di: DialogInterface, _: Int -> }
//            builder.show()
        }
        val count = carts.size
        Utilities.saveInt(context, "TotalCartItem", count)
        holder.btn_minus.setOnClickListener {
            number = holder.tv_quantity.text.toString().toInt()
            if (number <= 1) {
                number = 1
            } else {
                number--
                holder.tv_quantity.text = "" + number
            }
            val price: String = cart.price
            val value1 = if (price.isEmpty()) 0.0 else price.toDouble()
            val quantity = holder.tv_quantity.text.toString()
            val value2 = if (quantity.isEmpty()) 0.0 else quantity.toDouble()
            val a: Double
            a = value1 * value2
            val finalValue = java.lang.Double.toString(a)
            holder.tv_Price.text = finalValue
            myAppDatabase.cartDao().update(quantity, carts[position].id)
            cartClickListner.onMinusClick(cart)
        }
        holder.btn_add.setOnClickListener {
            number = holder.tv_quantity.text.toString().toInt()
            number++
            holder.tv_quantity.text = "" + number
            val price: String = cart.price
            val value1 = if (price.isEmpty()) 0.0 else price.toDouble()
            val quantity = holder.tv_quantity.text.toString()
            val value2 = if (quantity.isEmpty()) 0.0 else quantity.toDouble()
            val a: Double
            a = value1 * value2
            val finalValue = java.lang.Double.toString(a)
            holder.tv_Price.text = finalValue
            myAppDatabase.cartDao().update(quantity, carts[position].id)
            cartClickListner.onPlusClick(cart)
        }
    }

    override fun getItemCount(): Int {
        return carts.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
         val tv_quantity: TextView
         val tv_Name: TextView
         val tv_Price: TextView
         val tv_productdesc: TextView
         val btn_add: ImageView
         val btn_minus: ImageView
        var ivCross: ImageView

        init {
            tv_quantity = itemView.findViewById(R.id.tv_q_cart)
            ivCross = itemView.findViewById(R.id.img_delete)
            btn_add = itemView.findViewById(R.id.img_max)
            btn_minus = itemView.findViewById(R.id.img_min)
            tv_Name = itemView.findViewById(R.id.tv_productName)
            tv_Price = itemView.findViewById(R.id.tv_productPrice)
            tv_productdesc = itemView.findViewById(R.id.tv_productdesc)
        }
    }

    init {
        this.carts = carts
        this.context = context
        cartClickListner = listener
        myAppDatabase = Room.databaseBuilder(context, MyDatabase::class.java, "My_Cart")
            .allowMainThreadQueries().build()
    }
}

