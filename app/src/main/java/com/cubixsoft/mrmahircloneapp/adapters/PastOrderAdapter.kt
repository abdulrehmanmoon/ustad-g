package com.cubixsoft.mrmahircloneapp.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.cubixsoft.mrmahircloneapp.HomeActivity
import com.cubixsoft.mrmahircloneapp.R
import com.cubixsoft.mrmahircloneapp.Utilities
import com.myapps.healthapp1.models.subCategories.PastOrderDataModel
import com.wang.avi.AVLoadingIndicatorView
import android.os.Bundle




class PastOrderAdapter(
    private val context: Context,
    private val raceModel: List<PastOrderDataModel>?,
) :
    RecyclerView.Adapter<PastOrderAdapter.ViewHolder>() {


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.textView)
        val tvAmount: TextView = itemView.findViewById(R.id.tvAmount)
        val tvStatus: TextView = itemView.findViewById(R.id.tvStatus)
        val tvTimeDATE: TextView = itemView.findViewById(R.id.tvTimeDATE)
        val image: ImageView = itemView.findViewById(R.id.profile_image)
        val imageLoading: AVLoadingIndicatorView = itemView.findViewById(R.id.imageLoading)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.row_order_list, parent, false)
        return ViewHolder(itemView)
    }

    @SuppressLint("SimpleDateFormat", "SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        //
        val model = raceModel?.get(position)
        if (model != null) {
//            holder.title.text = model.name
            holder.tvTimeDATE.text = model.date + " " + model.time
            holder.tvAmount.text = model.total_amount.toString()
            holder.tvStatus.text = model.status
            // sets the image to the imageview from our itemHolder class

        }




        holder.itemView.setOnClickListener {
            Utilities.saveString(context, "subCategoryId", model?.id.toString())
//            Utilities.saveString(context,"title", model?.name.toString())
            Utilities.saveString(context, "title", holder.title.text.toString())

            Utilities.saveInt(context, "position", position)
//            Utilities.setPastBookingModels(context,raceModel)
            Utilities.setOrderDetailServicesModel(context, raceModel)
            val args = Bundle()
            val userProfileString: String = model?.getAddress.toString()
            args.putString("userProfileString", userProfileString)
//            context.fragmentUserProfile.setArguments(args)
            (context as HomeActivity).navController.navigate(R.id.action_myOrderFragment_to_orderDetailFragment)
//            Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_categoryFragment)

        }

    }


    override fun getItemCount() = raceModel!!.size

}
