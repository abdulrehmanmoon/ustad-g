package com.cubixsoft.mrmahircloneapp

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.cubixsoft.mehrancashcarry.models.ItemsViewModel


class SingleQuantityAdapter(
    var context: Context,
    var raceModel: List<ItemsViewModel>,
    var callback: Callback
) :
    RecyclerView.Adapter<SingleQuantityAdapter.ViewHolder>() {

    override fun onCreateViewHolder(container: ViewGroup, i: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_layout,
                container,
                false
            )
        )
    }

    private var selectedPosition = -1
    override fun onBindViewHolder(holder: ViewHolder, i: Int) = holder.bind(i)

    override fun getItemCount(): Int = raceModel.size

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var servicename = itemView.findViewById(R.id.servicename) as TextView
        var tvQuantuty = itemView.findViewById(R.id.tvQuantuty) as TextView
        var llTop = itemView.findViewById(R.id.llMain) as LinearLayout

        @SuppressLint("ResourceAsColor")
        fun bind(pos: Int) {
            val model = raceModel[position]
            tvQuantuty.text = model.text
            if (selectedPosition == position) {
                llTop.isSelected = true //using selector drawable
                servicename.setTextColor(
                    ContextCompat.getColor(
                        servicename.getContext(),
                        R.color.white
                    )
                )
                tvQuantuty.setTextColor(
                    ContextCompat.getColor(
                        tvQuantuty.getContext(),
                        R.color.white
                    )
                )
                llTop.background = context.getDrawable(R.drawable.round_corner_shape_primary)
//            holder.imagebg.visibility = View.GONE
//            holder.imgSELECT.visibility = View.VISIBLE

            } else {
                llTop.isSelected = false
                servicename.setTextColor(
                    ContextCompat.getColor(
                        servicename.getContext(),
                        R.color.black
                    )
                )
                llTop.background = context.getDrawable(R.drawable.round_border_style)

                tvQuantuty.setTextColor(
                    ContextCompat.getColor(
                        tvQuantuty.getContext(),
                        R.color.black
                    )
                )

            }

            llTop.setOnClickListener { v ->
                if (selectedPosition >= 0) notifyItemChanged(selectedPosition)
                selectedPosition = adapterPosition
                notifyItemChanged(selectedPosition)
//                Utilities.saveString(context, "pickLocation", model.pickup_point)
                callback.onItemClicked(adapterPosition)
//            }
            }

        }

    }
    interface Callback {
        fun onItemClicked(pos: Int)
        fun onDeleteClicked(pos: Int)
        fun oncvItemClicked(pos: Int)
    }
}