package com.cubixsoft.mrmahircloneapp.services


import com.cubixsoft.mrmahircloneapp.models.*
import com.cubixsoft.mrmahircloneapp.models.addresses.MainAddressResponse
import com.cubixsoft.mrmahircloneapp.models.categories.MainCategResponse
import com.cubixsoft.mrmahircloneapp.models.notifications.MainNotificationResponse
import com.cubixsoft.mrmahircloneapp.models.slider.MainSliderResponse
import com.cubixsoft.mrmahircloneapp.models.trending.MainTrendingResponse
import com.google.gson.JsonObject
import com.myapps.healthapp1.models.subCategories.MainOrderResponse
import com.myapps.healthapp1.models.subCategories.MainServiceResponse
import com.myapps.healthapp1.models.subCategories.MainSubCategResponse
import retrofit2.Call
import retrofit2.http.*


interface ApiService {
    @POST("login")

    fun login(
        @Query("email") email: String?,
        @Query("password") password: String?,
    ):
            Call<UserResponse>
    @POST("add_address")

    fun add_address(
        @Query("user_id") user_id: String?,
        @Query("title") title: String?,
        @Query("string") address: String?,
        @Query("lat") lat: String?,
        @Query("lang") lang: String?,



    ):
            Call<CommonResponseInt>

    @POST("points")
    @FormUrlEncoded
    fun getPoints(
        @Field("user_id") email: String?,
    ):
            Call<PointsResponse>


    @POST("update_profile_image")
    @FormUrlEncoded
    fun updateprofileImage(
        @Field("user_id") userId: String?,
        @Field("image") image: String?,
    ):
            Call<UserProfileImageResponse>

    @POST("update_profile")
    @FormUrlEncoded
    fun updateprofile(
        @Field("user_id") userId: String?,
        @Field("name") name: String?,
        @Field("email") email: String?,
    ):
            Call<UserResponse>

//    @POST("forgotPassword")
//    @FormUrlEncoded
//    fun forgotPasswordApi(
//        @Field("email") email: String?,
//    ):
//            Call<ForgotPassResponse>

    @POST("register")
    @FormUrlEncoded
    fun signUp(
        @Field("name") name: String?,
        @Field("phone") phone: String?,
        @Field("email") email: String?,
        @Field("password") password: String?,
    ):
            Call<CommonResponse>

    @POST("categories")
    @FormUrlEncoded
    fun getCategory(@Field("city") city: String?, @Field("domain") domain: String?):
            Call<MainCategResponse>

    @POST("domains")
    @FormUrlEncoded
    fun getDomains(@Field("city") city: String?):
            Call<MainDomainResponse>
    @POST("address-list")
    @FormUrlEncoded
    fun getAaddress_list(@Field("user_id") user_id: String?):
            Call<MainAddressResponse>

    @POST("notifications")
    @FormUrlEncoded
    fun getnotifications(@Field("user_id") city: String?):
            Call<MainNotificationResponse>

    @POST("trending")
    @FormUrlEncoded
    fun gettrendingApi(@Field("city") city: String?, @Field("domain") category: String?):
            Call<MainTrendingResponse>

    @POST("order")
    fun checkout(@Body params: JsonObject?): Call<CommonResponse>

    @POST("banner")
    @FormUrlEncoded
    fun getSliderApi(@Field("city") city: String?, @Field("domain") category: String?):
            Call<MainSliderResponse>

    @POST("sub_categories")
    @FormUrlEncoded
    fun getSubCategoryApi(@Field("category_id") category_id: String?,@Field("city") city: String?):
            Call<MainSubCategResponse>

    @POST("order-list")
    @FormUrlEncoded
    fun getPastorderList(@Field("user_id") user_id: String?,@Field("status") status: String?):
            Call<MainOrderResponse>


    @POST("services")
    @FormUrlEncoded
    fun getService(@Field("sub_category_id") category_id: String?,@Field("city") city: String?):
            Call<MainServiceResponse>


    @POST("search")
    @FormUrlEncoded
    fun getsearchApi(@Field("name") name: String?,@Field("city") city: String?,@Field("domain") domain: String?):
            Call<MainServiceResponse>
//    @POST("changePassword")
//    @FormUrlEncoded
//    fun changePassword(
//        @Field("email") email: String?,
//        @Field("new_password") password: String?,
//    ):
//            Call<CommonResponse>
//
//
//
//    @POST("updateProfile")
//    @FormUrlEncoded
//    fun updateProfileApi(
//        @Field("user_id") user_id: String?,
//        @Field("first_name") first_name: String?,
//        @Field("last_name") last_name: String?,
//        @Field("email") email: String?,
//    ):
//            Call<UserResponse>
//
//    @POST("updatePassword")
//    @FormUrlEncoded
//    fun changePassApi(
//        @Field("user_id") user_id: String?,
//        @Field("old_password") old_password: String?,
//        @Field("new_password") new_password: String?,
//    ):
//            Call<UserResponse>
//
//
//    @POST("checkIn")
//    @FormUrlEncoded
//    fun checkIn(
//        @Field("previous_weight") previous_weight: String?,
//        @Field("current_weight") current_weight: String?,
//        @Field("comment") comment: String?,
//        @Field("image_1") image_1: String?,
//        @Field("image_2") image_2: String?,
//        @Field("image_3") image_3: String?,
//        @Field("image_4") image_4: String?,
//        @Field("user_id") user_id: String?,
//    ):
//            Call<CheckinResponse>
//
//
//    @POST("addImage")
//    @FormUrlEncoded
//    fun addImage(
//        @Field("previous_weight") previous_weight: String?,
//        @Field("current_weight") current_weight: String?,
//        @Field("image") image: String?,
//        @Field("user_id") user_id: String?,
//    ):
//            Call<CommonResponse>
//
//
//    @POST("getImages")
//    @FormUrlEncoded
//    fun getImages(
//        @Field("user_id") user_id: String?,
//        @Field("month") month: String?,
//    ):
//            Call<MainGetImageResponse>
//
//    @POST("downloadPdf")
//    @FormUrlEncoded
//    fun downloadPdf(
//        @Field("type") type: String?
//    ):
//            Call<MainDownloadPDFResponse>
//
//    @GET("getNotifications")
//    fun getNotifications(
//    ):
//            Call<MainNotificationResponse>
}