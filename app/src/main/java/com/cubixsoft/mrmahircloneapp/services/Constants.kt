package com.cubixsoft.mrmahircloneapp.services


object Constants {

    // Endpoints
    const val BASE_URL = "https://bitborgtech.com/api/"
//    const val BASE_URL = "http://jsonplaceholder.typicode.com/"
//    const val BASE_URL_IMG = "http://145.14.158.138/fitness-app/public/"
    const val BASE_URL_IMG = "https://bitborgtech.com/"

    const val LOGIN_URL = "login"
    const val POSTS_URL = "posts"

}