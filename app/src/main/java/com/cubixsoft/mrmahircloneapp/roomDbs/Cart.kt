package com.cubixsoft.mrmahircloneapp.roomDbs

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "MyCart")
class Cart(
    @field:ColumnInfo(name = "id") @field:PrimaryKey(autoGenerate = true) var id: Int,
    @field:ColumnInfo(
        name = "subcategoryId"
    ) var subCategoryId: String,
    @field:ColumnInfo(name = "name") var name: String,
    @field:ColumnInfo(
        name = "quantity"
    ) var quantity: String,
    @field:ColumnInfo(name = "price") var price: String,
    @field:ColumnInfo(
        name = "desc"
    ) var desc: String,
    @field:ColumnInfo(name = "image") var image: String,
    @field:ColumnInfo(
        name = "prodcut_currency"
    ) var prodcut_currency: String,
)
