package com.cubixsoft.mrmahircloneapp.roomDbs;


import androidx.room.Database;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;


@Database(entities={Cart.class,FavStoreModel.class},version = 5)
public abstract class MyDatabase extends RoomDatabase{
        public abstract CartDao cartDao();

        @Override
        protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
            return null;
        }

        @Override
        protected InvalidationTracker createInvalidationTracker() {
            return null;
        }
    }

