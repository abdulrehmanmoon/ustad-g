package com.cubixsoft.mrmahircloneapp.roomDbs

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "MyFav")
class FavStoreModel(
    @field:PrimaryKey var storeId: Int,
    @field:ColumnInfo(
        name = "name"
    ) var name: String,
    @field:ColumnInfo(name = "catName") var catName: String,
    @field:ColumnInfo(
        name = "minPurchase"
    ) var minPurchase: String,
    @field:ColumnInfo(name = "currency") var currency: String,
    @field:ColumnInfo(
        name = "image"
    ) var image: String,
    @field:ColumnInfo(name = "isFavourite") var isFav: String
)