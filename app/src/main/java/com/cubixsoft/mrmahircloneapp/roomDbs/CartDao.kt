package com.cubixsoft.mrmahircloneapp.roomDbs

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface CartDao {
    @Insert
    fun insertAll(cart: Cart?)

    @get:Query("SELECT * FROM MyCart")
    val allCartItem: List<Cart?>?

    @Query("SELECT EXISTS (SELECT 1 FROM MyCart WHERE id=:id)")
    fun isAddToCart(id: Int): Int

    @Query("select COUNT (*) from MyCart")
    fun countCart(): Int

    @Query("DELETE FROM MyCart WHERE id=:id")
    fun deleteItem(id: Int)

    @Query("DELETE FROM MyCart")
    fun deleteAll()

    @Query("UPDATE MyCart SET quantity = :quantity ,price= :price  WHERE id LIKE :id ")
    fun updateItem(id: Int, quantity: String?, price: String?): Int




    @Query("UPDATE MyCart SET quantity =:quantity WHERE id = :id")
    fun update(quantity: String?, id: Int)

    @Query("SELECT EXISTS (SELECT 1 FROM MyCart WHERE id = :id)")
    fun checkProduct(id: Int): Boolean

    @Query("SELECT quantity from MyCart WHERE  id = :id")
    fun getquantity(id: Int): String?
}
