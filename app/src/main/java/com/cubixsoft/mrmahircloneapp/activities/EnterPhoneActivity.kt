package com.cubixsoft.mrmahircloneapp.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.cubixsoft.mrmahircloneapp.R
import com.cubixsoft.mrmahircloneapp.base.BaseActivityWithoutVM
import com.cubixsoft.mrmahircloneapp.databinding.ActivityEnterPhoneBinding
import com.cubixsoft.mrmahircloneapp.databinding.ActivityLogInBinding
import com.cubixsoft.mrmahircloneapp.services.ApiClient

class EnterPhoneActivity : BaseActivityWithoutVM<ActivityEnterPhoneBinding>() {

    private lateinit var apiClient: ApiClient
    private lateinit var newToken: String
    lateinit var loginStatus: String
    override fun getViewBinding(): ActivityEnterPhoneBinding =
        ActivityEnterPhoneBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)

        mViewBinding.apply {
            bContinue.setOnClickListener {

                val intent = Intent(this@EnterPhoneActivity, SignupActivity::class.java)
                startActivity(intent)
            }
        }
    }
}