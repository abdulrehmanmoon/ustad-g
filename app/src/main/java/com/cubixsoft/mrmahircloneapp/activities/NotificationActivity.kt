package com.cubixsoft.mrmahircloneapp.activities

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.recyclerview.widget.GridLayoutManager
import com.cubixsoft.mrmahircloneapp.Utilities
import com.cubixsoft.mrmahircloneapp.adapters.NotificationAdapter
import com.cubixsoft.mrmahircloneapp.base.BaseActivityWithoutVM
import com.cubixsoft.mrmahircloneapp.databinding.ActivityNotficationBinding
import com.cubixsoft.mrmahircloneapp.models.notifications.MainNotificationResponse
import com.cubixsoft.mrmahircloneapp.models.notifications.NotificationDataModel
import com.cubixsoft.mrmahircloneapp.services.ApiClient
import com.google.android.material.navigation.NavigationView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationActivity : BaseActivityWithoutVM<ActivityNotficationBinding>() {
    private var drawer: DrawerLayout? = null
    var logo: ImageView? = null
    private lateinit var apiClient: ApiClient
    var listModel: List<NotificationDataModel>? = null
    lateinit var navController: NavController
    var navigationView: NavigationView? = null
    override fun getViewBinding(): ActivityNotficationBinding =
        ActivityNotficationBinding.inflate(layoutInflater)

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    @SuppressLint("UseCompatLoadingForDrawables")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)
//        Utilities.saveString(this, "userCityName", cityName)
//        Utilities.saveString(this, "userLatitude", mLocation!!.latitude.toString())
//        Utilities.saveString(this, "userLongitude", mLocation!!.longitude.toString())
        val userid: String = Utilities.getString(this, Utilities.USER_ID)

        val address: String = Utilities.getString(this, "userAddress")
        val userCityName: String = Utilities.getString(this, "userCityName")
        mViewBinding.apply {
            ivBack.setOnClickListener {
                onBackPressed()
            }
        }
        apiClient = ApiClient()
        callApi("6")
    }

    override fun onResume() {
        super.onResume()
        val profileImage: String =
            Utilities.getString(this, Utilities.USER_PROFILE_IMAGE)

    }

    fun callApi(userId: String) {
        apiClient.getApiService().getnotifications(userId)
            .enqueue(object : Callback<MainNotificationResponse> {
                override fun onFailure(call: Call<MainNotificationResponse>, t: Throwable) {
                    Toast.makeText(
                        this@NotificationActivity,
                        "failure case:   " + t.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()
                    mViewBinding.shimmerViewContainer.visibility = View.GONE

//                    tvStatus.visibility = View.VISIBLE
                }

                override fun onResponse(
                    call: Call<MainNotificationResponse>,
                    response: Response<MainNotificationResponse>,
                ) {
                    val loginResponse = response.body()

                    if (loginResponse?.statusCode == true) {
                        listModel = response.body()!!.getData
                        Toast.makeText(
                            this@NotificationActivity,
                            "" + loginResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()
                        mViewBinding.shimmerViewContainer.visibility = View.GONE
                        mViewBinding.rvList.apply {
                            setHasFixedSize(true)
                            layoutManager =
                                GridLayoutManager(this@NotificationActivity,
                                    1,
                                    GridLayoutManager.VERTICAL,
                                    false)
//                            adapter = activity?.let { SuggestedAdapterNew(sugestedmodel, it) };
                            adapter = NotificationAdapter(
                                this@NotificationActivity, listModel
                            )
                            mViewBinding.rvList.setHasFixedSize(true)
//                            mViewBinding.tvStatus.visibility = View.GONE

                        }
                    } else {
//                        val loginResponse = response.body()
                        if (loginResponse != null) {
                            Toast.makeText(
                                this@NotificationActivity,
                                "" + loginResponse.message,
                                Toast.LENGTH_SHORT
                            ).show()
                            mViewBinding.shimmerViewContainer.visibility = View.GONE

//                            mViewBinding.tvStatus.visibility = View.VISIBLE

                        }
                    }
                }
            })
    }
}