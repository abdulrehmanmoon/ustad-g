package com.cubixsoft.mrmahircloneapp.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.cubixsoft.mrmahircloneapp.MainActivity;
import com.cubixsoft.mrmahircloneapp.R;
import com.cubixsoft.mrmahircloneapp.Utilities;
import com.cubixsoft.mrmahircloneapp.adapters.MyInfoWindowAdapter;
import com.cubixsoft.mrmahircloneapp.utils.GPSTracker;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Locale;

public class LocationActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap myGoogleMap;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private Location currentlocation;
    private AutocompleteSupportFragment autocompleteFragment;
    private static final int REQUEST_CODE = 103;
    private LatLng selectedLatLng;
    private LinearLayout buttonCurrentLocation;
    String address = "";
    String latitude, longitude;
    TextView buttonSaveLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        ImageView ivBack = findViewById(R.id.button_back);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        GPSTracker gpsTracker = new GPSTracker(this);
        if (gpsTracker.getIsGPSTrackingEnabled()) {
            latitude = String.valueOf(gpsTracker.getLatitude());
            longitude = String.valueOf(gpsTracker.getLongitude());

//            Utilities.saveString(LocationActivity.this, "latitude_near", latitude);
//            Utilities.saveString(LocationActivity.this, "longitude_near", longitude);

//            Toast.makeText(HomeActivity.this, latitude + " " + longitude, Toast.LENGTH_SHORT).show();
        } else {
            gpsTracker.showSettingsAlert();
        }
        buttonSaveLocation = findViewById(R.id.btn_save_location);
        buttonCurrentLocation = findViewById(R.id.ll_current_location);


        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());
        fetchLastLocation();


        //For AutoComplete Search Places
        Places.initialize(getApplicationContext(), Utilities.MAP_KEY, Locale.getDefault());
        PlacesClient placesClient = Places.createClient(getApplicationContext());
        // Initialize the AutocompleteSupportFragment.
        autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        // Specify the types of place data to return.
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.NAME, Place.Field.LAT_LNG));

        buttonCurrentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fetchLastLocation();

                address = Utilities.getAddress(getApplicationContext(), selectedLatLng);
                Utilities.saveString(LocationActivity.this, "getAddress", address);
                addMarkerOnMap(selectedLatLng, address);
                autocompleteFragment.setText(address);

            }
        });

        buttonSaveLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedLatLng == null) {
                    Toast.makeText(LocationActivity.this, "First Select the Location", Toast.LENGTH_SHORT).show();
                } else {

//                    finish();
//                    Utilities.saveString(LocationActivity.this,"getAddress",address);

                    fetchLastLocation();
                    Intent intent = new Intent(LocationActivity.this, MainActivity.class);
                    saveLatLng(selectedLatLng);
                    address = Utilities.getAddress(getApplicationContext(), selectedLatLng);
                    Utilities.saveString(LocationActivity.this, "getAddress", address);
                    addMarkerOnMap(selectedLatLng, address);
                    autocompleteFragment.setText(address);
                    Utilities.saveString(LocationActivity.this, "savedAddress", "yes");
                    startActivity(intent);
                    finish();

                }

            }
        });

    }


    private void fetchLastLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
            return;
        }

        Task<Location> task = fusedLocationProviderClient.getLastLocation()
                .addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {

                        if (task.isSuccessful()) {

                            currentlocation = task.getResult();
                            selectedLatLng = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));

                            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.google_map);
                            mapFragment.getMapAsync(LocationActivity.this::onMapReady);

                        }
                    }
                });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        myGoogleMap = googleMap;

        //Setting the Custom Marker Title
        myGoogleMap.setInfoWindowAdapter(new MyInfoWindowAdapter(getApplicationContext()));

        //First show the Selected Location that is stored in the SharedPref, If no location is saved
        //then show the current location
        // showSavedLatLng returns TRUE if the savedLocation is successfully shown on map otherwise FALSE
        if (!showSavedLatLng()) {
            selectedLatLng = new LatLng(currentlocation.getLatitude(), currentlocation.getLongitude());
            String address = Utilities.getAddress(getApplicationContext(), selectedLatLng);
            addMarkerOnMap(selectedLatLng, address);
        }

        myGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                selectedLatLng = latLng;
                String address = Utilities.getAddress(getApplicationContext(), latLng);

                addMarkerOnMap(selectedLatLng, address);

                autocompleteFragment.setText(address);


            }
        });


        myGoogleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {

                selectedLatLng = marker.getPosition();
                saveLatLng(selectedLatLng);
                String address = Utilities.getAddress(getApplicationContext(), selectedLatLng);

                addMarkerOnMap(selectedLatLng, address);

                autocompleteFragment.setText(address);


            }
        });

        // Set up a PlaceSelectionListener to handle the response.
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NotNull Place place) {
                // TODO: Get info about the selected place.

                selectedLatLng = place.getLatLng();

                if (selectedLatLng != null) {

                    String address = Utilities.getAddress(getApplicationContext(), selectedLatLng);
                    Utilities.saveString(LocationActivity.this, "getAddress", address);
                    addMarkerOnMap(selectedLatLng, address);
                }

            }


            @Override
            public void onError(@NotNull Status status) {
                // TODO: Handle the error.
                Log.i("YAM", "An error occurred: " + status);
            }
        });

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

        Toast.makeText(this, "hasCaptured : " + hasCapture, Toast.LENGTH_SHORT).show();

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                fetchLastLocation();
            }
        }
    }

    private void addMarkerOnMap(LatLng latLng, String address) {

        Drawable circleDrawable = getResources().getDrawable(R.drawable.ic_map_pin);
        BitmapDescriptor markerIcon = Utilities.getMarkerIconFromDrawable(circleDrawable);


        MarkerOptions markerOptions = new MarkerOptions()
                .position(latLng)
                .title(address)
                .icon(markerIcon)
                .snippet("Please move the marker if needed.")
                .draggable(true);

        myGoogleMap.clear();

        //myGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        // myGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));

        //This will move the marker with animation
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng)
                .zoom(16)
                .build();
        CameraUpdate cu = CameraUpdateFactory.newCameraPosition(cameraPosition);
        myGoogleMap.animateCamera(cu);

        Marker marker = myGoogleMap.addMarker(markerOptions);
        marker.showInfoWindow();
    }

    public void saveLatLng(LatLng latLng) {

        if (latLng != null) {

            Utilities.saveString(getApplicationContext(), Utilities.SELECTED_LAT, String.valueOf(latLng.latitude));
            Utilities.saveString(getApplicationContext(), Utilities.SELECTED_LON, String.valueOf(latLng.longitude));
        } else {
            Toast.makeText(LocationActivity.this, "location not found", Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    protected void onResume() {
        showSavedLatLng();
        super.onResume();
    }

    public Boolean showSavedLatLng() {

        Boolean shown = false;
        String lat = Utilities.getString(getApplicationContext(), "userLatitude");
        String lon = Utilities.getString(getApplicationContext(), "userLongitude");

        if (!TextUtils.isEmpty(lat) && !TextUtils.isEmpty(lon) && myGoogleMap != null) {
            LatLng userLatLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lon));
            String address = Utilities.getAddress(getApplicationContext(), userLatLng);
            addMarkerOnMap(userLatLng, address);
            shown = true;
        }
        return shown;
    }

}