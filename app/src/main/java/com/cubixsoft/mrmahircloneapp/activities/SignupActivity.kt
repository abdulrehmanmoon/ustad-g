package com.cubixsoft.mrmahircloneapp.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.cubixsoft.mrmahircloneapp.base.BaseActivityWithoutVM
import com.cubixsoft.mrmahircloneapp.databinding.ActivitySignupBinding
import com.cubixsoft.mrmahircloneapp.models.CommonResponse
import com.cubixsoft.mrmahircloneapp.services.ApiClient
import com.kaopiz.kprogresshud.KProgressHUD
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignupActivity : BaseActivityWithoutVM<ActivitySignupBinding>() {
    private lateinit var newToken: String
    private lateinit var apiClient: ApiClient
    override fun getViewBinding(): ActivitySignupBinding =
        ActivitySignupBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)

        apiClient = ApiClient()
//        FirebaseInstanceId.getInstance().getToken()
//
//        FirebaseInstanceId.getInstance().getInstanceId()
//            .addOnSuccessListener(this) { instanceIdResult ->
//                newToken = instanceIdResult.getToken()
//                Log.e("newToken", newToken)
//            }

        mViewBinding.apply {
            tvLogin.setOnClickListener {
                val intent = Intent(this@SignupActivity, LogInActivity::class.java)
                startActivity(intent)
            }
            bSignUp.setOnClickListener {

                val getFName: String = etFullName.text.toString()
                val phone: String = etPhone.text.toString()
                val getEmail: String = ettEmail.text.toString()
                val getPass: String = editPassword.text.toString()
                if (!getFName.equals("")) {
                    if (!phone.equals("")) {
                        if (!getEmail.equals("")) {
                            if (android.util.Patterns.EMAIL_ADDRESS.matcher(getEmail).matches()) {
                                if (!getEmail.equals("")) {
                                    callApi(getFName, phone, getEmail, getPass)

                                } else {
                                    showToast("Enter Password")
                                }
                            } else {
                                showToast("Enter Valid Email Address")

                            }
                        } else {
                            showToast("Enter Email Address")

                        }
                    } else {
                        showToast("Enter Last Name")

                    }
                } else {
                    showToast("Enter First Address")

                }
            }

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_log_in)
        }
    }

    fun callApi(fName: String, phone: String, email: String, pass: String) {
        val hud = KProgressHUD.create(this)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel("Please wait")
            .setCancellable(true)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)
            .show()

        apiClient.getApiService().signUp(fName, phone, email, pass)
            .enqueue(object : Callback<CommonResponse> {
                override fun onFailure(call: Call<CommonResponse>, t: Throwable) {
                    hud.dismiss()
                    Toast.makeText(applicationContext, "false" + t.message, Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onResponse(
                    call: Call<CommonResponse>,
                    response: Response<CommonResponse>,
                ) {
                    val loginResponse = response.body()

                    if (loginResponse?.statusCode == true) {
                        hud.dismiss()
                        showToast(loginResponse.message)
                        val intent = Intent(this@SignupActivity, LogInActivity::class.java)
                        startActivity(intent)
                        finish()
                    } else {
//                        val loginResponse = response.body()
                        hud.dismiss()
                        if (loginResponse != null) {
                            hud.dismiss()
                            showToast(loginResponse.message)
                        }
                    }
                }
            })
    }
}