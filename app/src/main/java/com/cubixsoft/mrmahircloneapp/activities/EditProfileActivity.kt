package com.cubixsoft.mrmahircloneapp.activities

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Base64
import android.view.View
import android.widget.Toast
import com.cubixsoft.mrmahircloneapp.R
import com.cubixsoft.mrmahircloneapp.Utilities
import com.cubixsoft.mrmahircloneapp.base.BaseActivityWithoutVM
import com.cubixsoft.mrmahircloneapp.databinding.ActivityEditProfleBinding
import com.cubixsoft.mrmahircloneapp.models.UserProfileImageResponse
import com.cubixsoft.mrmahircloneapp.models.UserResponse
import com.cubixsoft.mrmahircloneapp.services.ApiClient
import com.cubixsoft.mrmahircloneapp.services.Constants
import com.github.dhaval2404.imagepicker.ImagePicker
import com.kaopiz.kprogresshud.KProgressHUD
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File

class EditProfileActivity : BaseActivityWithoutVM<ActivityEditProfleBinding>() {

    private lateinit var apiClient: ApiClient
    private lateinit var newToken: String
    var uri1: Uri? = null
    lateinit var input: String
    var image: File? = null
    var selected_img_bitmap: Bitmap? = null
    lateinit var loginStatus: String
    override fun getViewBinding(): ActivityEditProfleBinding =
        ActivityEditProfleBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)
        apiClient = ApiClient()
        val profileImage: String = Utilities.getString(this, Utilities.USER_PROFILE_IMAGE)


        mViewBinding.apply {
            val userName: String = Utilities.getString(this@EditProfileActivity, Utilities.USER_FNAME)
            val userId: String = Utilities.getString(this@EditProfileActivity, Utilities.USER_ID)
            val userEmail: String = Utilities.getString(this@EditProfileActivity, Utilities.USER_EMAIL)
            if (!TextUtils.isEmpty(userName) || !TextUtils.isEmpty(userEmail)) {
                etFullName.setText(userName)
                etEmail.setText(userEmail)

            }
            if (!TextUtils.isEmpty(profileImage)) {
                Picasso.get()
                    .load(Constants.BASE_URL_IMG + profileImage).placeholder(R.drawable.profile_ic)
                    .into(mViewBinding.ivImage)

            } else {
                Picasso.get()
                    .load(R.drawable.profile_ic).placeholder(R.drawable.profile_ic)
                    .into(mViewBinding.ivImage)

            }

            bUpdateProfile.setOnClickListener {
                val getName: String = etFullName.text.toString()
                val getEmail: String = etEmail.text.toString()
                if (!TextUtils.isEmpty(getName)) {
                    if (!TextUtils.isEmpty(getEmail)) {

                        resetprofileApi(userId,getName,getEmail)

                    } else {
                        showToast("enter Email Address")

                    }
                } else {
                    showToast("enter Name")

                }
            }

            ivBack.setOnClickListener {
                onBackPressed()

            }
            llEditImage.setOnClickListener {
                ImagePicker.with(this@EditProfileActivity)
                    .crop()
                    .compress(1024)
                    .maxResultSize(1080, 1080)
                    .start()

            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            uri1 = data?.data!!
            //            Glide.with(this).load(uri1).into(binding.imagePicker)
            selected_img_bitmap =
                MediaStore.Images.Media.getBitmap(this.contentResolver, uri1)

            mViewBinding.ivImage.setImageBitmap(selected_img_bitmap)
            mViewBinding.ivAddPlus.visibility = View.GONE
            //You can get File object from intent
            val file: File = ImagePicker.getFile(data)!!
            //You can also get File Path from intent
            val filePath: String = ImagePicker.getFilePath(data)!!
            image = file

            val immagex: Bitmap = (selected_img_bitmap as Bitmap?)!!
            val byteArrayOutputStream = ByteArrayOutputStream()
            immagex.compress(
                Bitmap.CompressFormat.JPEG,
                70,
                byteArrayOutputStream
            )
            selected_img_bitmap?.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream)
            val byteArray = byteArrayOutputStream.toByteArray()
            val imageEncoded =
                Base64.encodeToString(byteArray, Base64.NO_WRAP)
            input = imageEncoded
            input = input.replace("\n", "")
            input = "data:image/png;base64," + input.trim { it <= ' ' }

            updateProfile(input)


        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }

    fun updateProfile(inputt: String) {
        val hud = KProgressHUD.create(this)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel("Please wait")
            .setCancellable(true)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)
            .show()

        apiClient.getApiService()
            .updateprofileImage(Utilities.getString(this, Utilities.USER_ID), inputt)
            .enqueue(object : Callback<UserProfileImageResponse> {
                override fun onFailure(call: Call<UserProfileImageResponse>, t: Throwable) {
                    hud.dismiss()
                    Toast.makeText(applicationContext, "false" + t.message, Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onResponse(
                    call: Call<UserProfileImageResponse>,
                    response: Response<UserProfileImageResponse>,
                ) {
                    val loginResponse = response.body()

                    if (loginResponse!!.status == 200) {
                        hud.dismiss()
                        showToast(loginResponse.message)
                        Utilities.saveString(
                            this@EditProfileActivity,
                            Utilities.USER_PROFILE_IMAGE,
                            loginResponse.user.profile_image.toString()
                        )

                        Picasso.get()
                            .load(Constants.BASE_URL_IMG + loginResponse.user.profile_image)
                            .into(mViewBinding.ivImage)

                    } else {
//                        val loginResponse = response.body()
                        hud.dismiss()
                        if (loginResponse != null) {
                            showToast(loginResponse.message)
                        }
                    }
                }
            })
    }

    fun resetprofileApi(userid: String, name: String, email: String) {
        val hud = KProgressHUD.create(this)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel("Please wait")
            .setCancellable(true)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)
            .show()

        apiClient.getApiService().updateprofile(userid, name,email)
            .enqueue(object : Callback<UserResponse> {
                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                    hud.dismiss()
                    Toast.makeText(applicationContext, "false" + t.message, Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onResponse(
                    call: Call<UserResponse>,
                    response: Response<UserResponse>,
                ) {
                    val loginResponse = response.body()

                    if (loginResponse!!.status == true) {
                        hud.dismiss()
                        showToast(loginResponse.message)
                        Utilities.saveString(
                            this@EditProfileActivity,
                            Utilities.USER_PROFILE_IMAGE,
                            loginResponse.user.profile_image.toString()
                        )
                        Utilities.saveString(
                            this@EditProfileActivity,
                            Utilities.USER_FNAME,
                            loginResponse.user.name.toString()
                        )
                        Utilities.saveString(
                            this@EditProfileActivity,
                            Utilities.USER_EMAIL,
                            loginResponse.user.email.toString()
                        )


                    } else {
//                        val loginResponse = response.body()
                        hud.dismiss()
                        if (loginResponse != null) {
                            showToast(loginResponse.message)
                        }
                    }
                }
            })
    }

}