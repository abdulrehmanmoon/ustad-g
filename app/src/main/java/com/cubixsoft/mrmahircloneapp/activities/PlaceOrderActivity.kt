package com.cubixsoft.mrmahircloneapp.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.format.DateFormat
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.cubixsoft.mehrancashcarry.models.ItemsViewModel
import com.cubixsoft.mrmahircloneapp.R
import com.cubixsoft.mrmahircloneapp.SingleQuantityAdapter
import com.cubixsoft.mrmahircloneapp.Utilities
import com.cubixsoft.mrmahircloneapp.base.BaseActivityWithoutVM
import com.cubixsoft.mrmahircloneapp.databinding.ActivityPlaceOrderBinding
import com.cubixsoft.mrmahircloneapp.models.CommonResponse
import com.cubixsoft.mrmahircloneapp.roomDbs.Cart
import com.cubixsoft.mrmahircloneapp.roomDbs.MyDatabase
import com.cubixsoft.mrmahircloneapp.services.ApiClient
import com.github.dhaval2404.imagepicker.ImagePicker
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.kaopiz.kprogresshud.KProgressHUD
import com.view.calender.horizontal.umar.horizontalcalendarview.DayDateMonthYearModel
import com.view.calender.horizontal.umar.horizontalcalendarview.HorizontalCalendarListener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


class PlaceOrderActivity : BaseActivityWithoutVM<ActivityPlaceOrderBinding>(),
    SingleQuantityAdapter.Callback , HorizontalCalendarListener {
    override fun getViewBinding(): ActivityPlaceOrderBinding =
        ActivityPlaceOrderBinding.inflate(layoutInflater)

    var input: String = ""
    var uri1: Uri? = null
    var image: File? = null
    var selected_img_bitmap: Bitmap? = null
    lateinit var ivImage: ImageView
    lateinit var ivAddPlus: ImageView
    lateinit var formated_time: String
    lateinit var datefFinal: String
    private val datas = mutableListOf<String>()
    var service_id: Int = 1
    var singleBuilder: SingleDateAndTimePickerDialog.Builder? = null
//    lateinit var horizontalCalendar: HorizontalCalendar
    var simpleDateOnlyFormat: SimpleDateFormat? = null
    lateinit var myDatabase: MyDatabase
    lateinit var itemList: List<Cart>
    private lateinit var apiClient: ApiClient

    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)
        simpleDateOnlyFormat =
            SimpleDateFormat("HH:mm a", Locale.getDefault())
        myDatabase = Room.databaseBuilder(
            this,
            MyDatabase::class.java, "My_Cart"
        ).allowMainThreadQueries().build()

        apiClient = ApiClient()
        itemList =
            myDatabase.cartDao().allCartItem as List<Cart>
        val startDate: Calendar = Calendar.getInstance()
        startDate.add(Calendar.MONTH, -1)
        val endDate: Calendar = Calendar.getInstance()
        endDate.add(Calendar.MONTH, 1)
        val defaultSelectedDate: Calendar = Calendar.getInstance()
//        horizontalCalendar = HorizontalCalendar.Builder(this, R.id.calendarView)
//            .range(startDate, endDate)
//            .datesNumberOnScreen(5)
//            .configure()
//            .formatTopText("MMM")
//            .formatMiddleText("dd")
//            .formatBottomText("EEE")
//            .showTopText(true)
//            .showBottomText(true)
//            .textColor(Color.BLACK, Color.BLACK)
//            .colorTextMiddle(Color.GREEN, Color.parseColor("#ffd54f"))
//            .end()
////            .defaultSelectedDate(defaultSelectedDate)
////            .addEvents(object : CalendarEventsPredicate {
////                var rnd: Random = Random()
////                override fun events(date: Calendar?): List<CalendarEvent>? {
////                    val events: MutableList<CalendarEvent> = ArrayList()
////                    val count: Int = rnd.nextInt(6)
////                    for (i in 0..count) {
////                        events.add(CalendarEvent(Color.rgb(rnd.nextInt(256),
////                            rnd.nextInt(256),
////                            rnd.nextInt(256)), "event"))
////                    }
////                    return events
////                }
////            })
//            .build()
//
////        Log.i("Default Date", DateFormat.format("EEE, MMM d, yyyy", defaultSelectedDate).toString())
//
//        horizontalCalendar.setCalendarListener(object : HorizontalCalendarListener() {
//            override fun onDateSelected(date: Calendar?, position: Int) {
//                val selectedDateStr: String = DateFormat.format("yyyy-MM", date).toString()
//                val daysOfMonth: String = DateFormat.format("dd", date).toString()
//                var finalinte: Int = daysOfMonth.toInt() + 1
//                datefFinal = selectedDateStr + "-" + finalinte
//                showToast(datefFinal)
//                Log.i("onDateSelected", "$date - Position = $position")
//            }
//        })
        val hcv = mViewBinding.horizontalcalendarview
        hcv.setContext(this)
        hcv.setBackgroundColor(resources.getColor(R.color.bg_gray))
        hcv.showControls(false)
        hcv.setControlTint(R.color.colorAccent)
        hcv.changeAccent(R.color.colorPrimary)
        mViewBinding.apply {
            ivImage = ivSetImage
            ivAddPlus = ivAddPlusss
            imagePicker.setOnClickListener {
                ImagePicker.with(this@PlaceOrderActivity)
                    .crop()
                    .compress(1024)
                    .maxResultSize(1080, 1080)
                    .start()

            }
            bPlaceorder.setOnClickListener {
                callCheckOutApi(Utilities.getString(this@PlaceOrderActivity, "userAddress"))
            }
            ivSetTimeClock.setOnClickListener {
                singleBuilder = SingleDateAndTimePickerDialog.Builder(this@PlaceOrderActivity)
//                    .bottomSheet()
                    .curved()
                    .backgroundColor(Color.WHITE)
                    .mainColor(Color.DKGRAY)
                    .titleTextColor(Color.WHITE)
                    .minutesStep(10)
                    .displayHours(true)
                    .displayMinutes(true)
                    .displayDays(false)
                    .displayListener(object : SingleDateAndTimePickerDialog.DisplayListener {
                        override fun onDisplayed(picker: SingleDateAndTimePicker) {

                        }


                    })
                    .title("Choose Time")
                    .listener(SingleDateAndTimePickerDialog.Listener { date ->
//                        val startDate: String = simpleDateOnlyFormat?.format(date).toString()
                        val myFormat = "hh:mm a" // your own format

                        val sdf = SimpleDateFormat(myFormat, Locale.US)
                        formated_time = sdf.format(date)

                        tvTimeDate.setText(formated_time)
                        Toast.makeText(this@PlaceOrderActivity, "" + date, Toast.LENGTH_LONG)
                            .show()

                    })
                singleBuilder!!.display()

            }
            singleRecyclerview.layoutManager =
                LinearLayoutManager(this@PlaceOrderActivity, LinearLayoutManager.HORIZONTAL, false)

            // ArrayList of class ItemsViewModel
            val data = java.util.ArrayList<ItemsViewModel>()
            for (i in 1..15) {
                data.add(ItemsViewModel(R.drawable.demo1, "$i "))

            }

            // This will pass the ArrayList to our Adapter
            val adapter =
                SingleQuantityAdapter(this@PlaceOrderActivity, data, this@PlaceOrderActivity)
            // Setting the Adapter with the recyclerview
            singleRecyclerview.adapter = adapter
        }


    }

    override fun onItemClicked(pos: Int) {

    }

    override fun onDeleteClicked(pos: Int) {
    }

    override fun oncvItemClicked(pos: Int) {
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            uri1 = data?.data!!
            //            Glide.with(this).load(uri1).into(binding.imagePicker)
            selected_img_bitmap =
                MediaStore.Images.Media.getBitmap(this.contentResolver, uri1)
            ivImage.setImageBitmap(selected_img_bitmap)
            ivAddPlus.visibility = View.GONE
            //You can get File object from intent
            val file: File = ImagePicker.getFile(data)!!
            //You can also get File Path from intent
            val filePath: String = ImagePicker.getFilePath(data)!!
            image = file
            val immagex: Bitmap = (selected_img_bitmap as Bitmap?)!!
            val byteArrayOutputStream = ByteArrayOutputStream()
            immagex.compress(
                Bitmap.CompressFormat.JPEG,
                70,
                byteArrayOutputStream
            )
            selected_img_bitmap?.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream)
            val byteArray = byteArrayOutputStream.toByteArray()
            val imageEncoded =
                Base64.encodeToString(byteArray, Base64.NO_WRAP)
            input = imageEncoded
            input = input.replace("\n", "")
            input = "data:image/png;base64," + input.trim { it <= ' ' }


        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }

    private fun callCheckOutApi(
        selected_address: String,
    ) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("user_id", "1")
        jsonObject.addProperty("date", datefFinal)
        jsonObject.addProperty("time", formated_time)
        jsonObject.addProperty("selected_address", 1)
        val array = JsonArray()
        for (i in itemList.indices) {
//            service_id = itemList.get(i).id
            service_id = 1
//            quantity = itemList.get(i).quantity.toInt()
//            price = itemList.get(i).price.toFloat()
//            sellerId = itemList.get(i).store_id
            val `object` = JsonObject()
            `object`.addProperty("service_id", service_id)
            array.add(`object`)
        }
        jsonObject.add("items", array)
        val hud = KProgressHUD.create(this)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel("Please wait")
            .setCancellable(true)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)
            .show()

//        val service: APIService = RetrofitClient.getRetrofitInstance()!!.create(
//            APIService::class.java
//        )
//        val call: Call<CheckOutResponseModel> = service.checkout(jsonObject)

        apiClient.getApiService().checkout(jsonObject)
            .enqueue(object : Callback<CommonResponse?> {
                override fun onResponse(
                    call: Call<CommonResponse?>,
                    response: Response<CommonResponse?>,
                ) {
                    assert(response.body() != null)
                    val status: Boolean = response.body()!!.statusCode
                    if (status == true) {
                        hud.dismiss()
                        Toast.makeText(
                            this@PlaceOrderActivity,
                            response.body()!!.message,
                            Toast.LENGTH_SHORT
                        ).show()
//                        myDatabase.cartDao().deleteAll()
//                    val intent =
//                        Intent(requireContext(), OrdersConfirmedActivity::class.java)
//                    startActivity(intent)

                    } else {
                        hud.dismiss()
                        Toast.makeText(
                            this@PlaceOrderActivity,
                            "Failed" + response.body()!!.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

                override fun onFailure(call: Call<CommonResponse?>, t: Throwable) {
                    hud.dismiss()
                    t.printStackTrace()
                }
            })
    }


    fun toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun getEncoded64ImageStringFromBitmap(bitmap: Bitmap): String? {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream)
        val byteFormat = stream.toByteArray()

        // Get the Base64 string
        return Base64.encodeToString(byteFormat, Base64.NO_WRAP)
    }
    override fun updateMonthOnScroll(selectedDate: DayDateMonthYearModel?) {
//        currentMonthTextView.text = ""+ selectedDate?.month + " " + selectedDate?.year
//        showToast(""+ selectedDate?.month + " " + selectedDate?.year)
        mViewBinding.tvMonth.text = "" + selectedDate?.month + " " + selectedDate?.year

    }

    override fun newDateSelected(selectedDate: DayDateMonthYearModel?) {
        Toast.makeText(this, selectedDate?.year + "-" +
                selectedDate?.monthNumeric + "-" + selectedDate?.date,
            Toast.LENGTH_LONG).show()
        var finalSelectedDate = selectedDate?.year + "-" +
                selectedDate?.day + "-" + selectedDate?.monthNumeric
        datefFinal = finalSelectedDate
    }
}