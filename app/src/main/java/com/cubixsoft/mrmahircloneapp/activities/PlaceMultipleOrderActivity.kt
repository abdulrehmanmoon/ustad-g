package com.cubixsoft.mrmahircloneapp.activities
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Base64
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.cubixsoft.mehrancashcarry.models.ItemsViewModel
import com.cubixsoft.mrmahircloneapp.HomeActivity
import com.cubixsoft.mrmahircloneapp.R
import com.cubixsoft.mrmahircloneapp.Utilities
import com.cubixsoft.mrmahircloneapp.adapters.CartAdapter
import com.cubixsoft.mrmahircloneapp.base.BaseActivityWithoutVM
import com.cubixsoft.mrmahircloneapp.databinding.ActivityMultiplePlaceOrderBinding
import com.cubixsoft.mrmahircloneapp.models.CommonResponse
import com.cubixsoft.mrmahircloneapp.roomDbs.Cart
import com.cubixsoft.mrmahircloneapp.roomDbs.MyDatabase
import com.cubixsoft.mrmahircloneapp.services.ApiClient
import com.cubixsoft.mrmahircloneapp.utils.CartClickListner
import com.github.dhaval2404.imagepicker.ImagePicker
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.kaopiz.kprogresshud.KProgressHUD
import com.view.calender.horizontal.umar.horizontalcalendarview.DayDateMonthYearModel
import com.view.calender.horizontal.umar.horizontalcalendarview.HorizontalCalendarListener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


class PlaceMultipleOrderActivity : BaseActivityWithoutVM<ActivityMultiplePlaceOrderBinding>(),
    CartClickListner, HorizontalCalendarListener {
    override fun getViewBinding(): ActivityMultiplePlaceOrderBinding =
        ActivityMultiplePlaceOrderBinding.inflate(layoutInflater)

    var myDatabase: MyDatabase? = null

    private var itemList: List<Cart>? = null
    var totaPriceee: TextView? = null
    var subtotaPrice: TextView? = null
    var tvTotaPrice: TextView? = null
    var input: String = ""
    var quantity: Int = 0
    var uri1: Uri? = null
    lateinit var ivImage: ImageView
    lateinit var ivAddPlus: ImageView
    var selected_img_bitmap: Bitmap? = null
     var formated_time: String? = null
    var image: File? = null
    lateinit var priceTrending: String;
    var cartRecyclerview: RecyclerView? = null
     var datefFinal: String? = null
    private val datas = mutableListOf<String>()
    var service_id: Int = 0
    var singleBuilder: SingleDateAndTimePickerDialog.Builder? = null

    //    lateinit var horizontalCalendar: HorizontalCalendar
    var simpleDateOnlyFormat: SimpleDateFormat? = null
    private var manager: LinearLayoutManager? = null
    var total = 0.0
    private lateinit var apiClient: ApiClient
    lateinit var data: ArrayList<ItemsViewModel>

    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)

        var title: String = Utilities.getString(this, "title")
        priceTrending = Utilities.getString(this, "priceTrending")

        simpleDateOnlyFormat =
            SimpleDateFormat("HH:mm a", Locale.getDefault())
        apiClient = ApiClient()

        myDatabase = Room.databaseBuilder(
            applicationContext,
            MyDatabase::class.java, "My_Cart").allowMainThreadQueries().build()

        val startDate: Calendar = Calendar.getInstance()
        startDate.add(Calendar.MONTH, -1)
        val endDate: Calendar = Calendar.getInstance()
        endDate.add(Calendar.MONTH, 1)
        val defaultSelectedDate: Calendar = Calendar.getInstance()
//        horizontalCalendar = HorizontalCalendar.Builder(this, R.id.calendarView)
//            .range(startDate, endDate)
//            .datesNumberOnScreen(6)
//            .configure()
//            .formatTopText("MMM")
//            .formatMiddleText("dd")
//            .formatBottomText("EEE")
//            .showTopText(true)
//            .showBottomText(true)
//            .textColor(Color.BLACK, Color.BLACK)
//            .colorTextMiddle(Color.GREEN, Color.parseColor("#ffd54f"))
//            .end()
////            .defaultSelectedDate(defaultSelectedDate)
////            .addEvents(object : CalendarEventsPredicate {
////                var rnd: Random = Random()
////                override fun events(date: Calendar?): List<CalendarEvent>? {
////                    val events: MutableList<CalendarEvent> = ArrayList()
////                    val count: Int = rnd.nextInt(6)
////                    for (i in 0..count) {
////                        events.add(CalendarEvent(Color.rgb(rnd.nextInt(256),
////                            rnd.nextInt(256),
////                            rnd.nextInt(256)), "event"))
////                    }
////                    return events
////                }
////            })
//            .build()
//
////        Log.i("Default Date", DateFormat.format("EEE, MMM d, yyyy", defaultSelectedDate).toString())
//
//        horizontalCalendar.setCalendarListener(object : HorizontalCalendarListener() {
//            override fun onDateSelected(date: Calendar?, position: Int) {
//                val selectedDateStr: String = DateFormat.format("yyyy-MM", date).toString()
//                val daysOfMonth: String = DateFormat.format("dd", date).toString()
//                var finalinte: Int = daysOfMonth.toInt() + 1
//                datefFinal = selectedDateStr + "-" + finalinte
//                showToast(datefFinal)
//                Log.i("onDateSelected", "$date - Position = $position")
//            }
//        })
//        var cal1 = Calendar.getInstance()
//        var cal2 = Calendar.getInstance()
//        var cal3 = Calendar.getInstance()
//        cal2.add(Calendar.DAY_OF_YEAR,4)
//        cal1.add(Calendar.DAY_OF_YEAR,2)
//        cal3.add(Calendar.DAY_OF_YEAR,-2)
//        var daysSelected = ArrayList<Date>()
//        daysSelected.add(cal1.time)
//        daysSelected.add(cal3.time)


//        HorizontalCalendar.Build(findViewById(R.id.calendarView),this,
//            BasicStyle(R.color.gray,R.color.dark_gray,R.color.red))
//            .colorText(BasicStyle(R.color.white,R.color.white,R.color.white))
//            .rangeMax(7,HorizontalCalendar.TIMEMEASURE.DAY)
//            .daysInScreen(7)
//            .onClickDay(this).
//            setGravityDaySelected(HorizontalCalendar.GRAVITY.CENTER)
//            .periodSelected(cal1.time,cal2.time,R.color.green,R.color.black)
//            .selectedDays(daysSelected,R.color.color_primary).build()
        val hcv = mViewBinding.horizontalcalendarview
        hcv.setContext(this)
        hcv.setBackgroundColor(resources.getColor(R.color.bg_gray))
        hcv.showControls(false)
        hcv.setControlTint(R.color.colorAccent)
        hcv.changeAccent(R.color.colorPrimary)

        mViewBinding.apply {
            noOfServices.text = "Select Quantity for  " + title
            subtotaPrice = tvUiSubtotal
            tvTotaPrice = tvUiTotalPrice
            totaPriceee = totalPrice
            cartRecyclerview = cartRecyclerviewww
            ivImage = ivSetImage
            ivAddPlus = ivAddPlusss
            imagePicker.setOnClickListener {
                ImagePicker.with(this@PlaceMultipleOrderActivity)
                    .crop()
                    .compress(1024)
                    .maxResultSize(1080, 1080)
                    .start()

            }
            bPlaceorder.setOnClickListener {
                if (!TextUtils.isEmpty(formated_time)) {
                    if (!TextUtils.isEmpty(datefFinal)) {
                        callCheckOutApi()

                    } else {
                        showToast("Choose Date")
                    }
                } else {
                    showToast("Select Time")
                }

            }

            ivBack.setOnClickListener {
                finish()
            }
            ivSetTimeClock.setOnClickListener {
                singleBuilder =
                    SingleDateAndTimePickerDialog.Builder(this@PlaceMultipleOrderActivity)
//                    .bottomSheet()
                        .curved()
                        .backgroundColor(Color.WHITE)
                        .mainColor(Color.DKGRAY)
                        .titleTextColor(Color.WHITE)
                        .minutesStep(10)
                        .displayHours(true)
                        .displayMinutes(true)
                        .displayDays(false)
                        .displayListener(object : SingleDateAndTimePickerDialog.DisplayListener {
                            override fun onDisplayed(picker: SingleDateAndTimePicker) {

                            }


                        })
                        .title("Choose Time")
                        .listener(SingleDateAndTimePickerDialog.Listener { date ->
//                        val startDate: String = simpleDateOnlyFormat?.format(date).toString()
                            val myFormat = "hh:mm" // your own format

                            val sdf = SimpleDateFormat(myFormat, Locale.US)
                            formated_time = sdf.format(date)

                            tvTimeDate.setText(formated_time)
//                            Toast.makeText(this@PlaceMultipleOrderActivity,
//                                "" + date,
//                                Toast.LENGTH_LONG)
//                                .show()

                        })
                singleBuilder!!.display()

            }
        }

        getCartitems()
    }


    private fun getCartitems() {
        total = 0.0
        itemList = ArrayList()
        itemList =
            myDatabase!!.cartDao().allCartItem as List<Cart>?
//        cartRecyclerview!!.setHasFixedSize(true)
        manager = LinearLayoutManager(this)
        cartRecyclerview!!.setLayoutManager(manager)
        val cartAdapter: CartAdapter = itemList?.let { CartAdapter(it, application, this) }!!
        cartRecyclerview!!.setAdapter(cartAdapter)
        if (itemList!!.size > 0) {
            calculatebill()
        } else {
            subtotaPrice?.setText("Rs 0.00")
            totaPriceee?.setText("Rs 0.00")
            tvTotaPrice?.setText("Rs 0.00")
//            tv_text.setText("Ckeckout 0.00 $")
//            tv_min_Orderprice.setText("0.00 $")
//            tvAddMore.setVisibility(View.GONE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            uri1 = data?.data!!
            //            Glide.with(this).load(uri1).into(binding.imagePicker)
            selected_img_bitmap =
                MediaStore.Images.Media.getBitmap(this.contentResolver, uri1)
            ivImage.setImageBitmap(selected_img_bitmap)
            ivAddPlus.visibility = View.GONE
            //You can get File object from intent
            val file: File = ImagePicker.getFile(data)!!
            //You can also get File Path from intent
            val filePath: String = ImagePicker.getFilePath(data)!!
            image = file
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }


    fun toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun getEncoded64ImageStringFromBitmap(bitmap: Bitmap): String? {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream)
        val byteFormat = stream.toByteArray()

        // Get the Base64 string
        return Base64.encodeToString(byteFormat, Base64.NO_WRAP)
    }


    private fun calculatebill() {
        for (i in 0..itemList!!.size - 1) {
            total = total + itemList!!.get(i).price.toDouble() * itemList!!.get(i).quantity
                .toDouble()
            subtotaPrice?.setText("Rs $total")
            totaPriceee?.setText("Rs $total")
            tvTotaPrice?.setText("Rs $total")
//            tvText.setText("Ckeckout $total $")
//            val min_store_price: Double = Utilities.getString(requireContext(), "storeMinPrice")!!.toDouble()
//            if (total >= min_store_price) {
////                binding.tvMinOrderprice.setText("0.0 $")
//                binding.tvAddMore.setVisibility(View.GONE)
//            } else {
//                val less_amount = min_store_price - total
////                binding.tvMinOrderprice.setText("$less_amount $")
//                binding.tvAddMore.setVisibility(View.VISIBLE)
//            }
        }
    }

//    protected override fun onResume() {
//        super.onResume()
//    }

    fun AddMore(view: View?) {
//        view?.findNavController()
//            ?.navigate(R.id.action_navigation_shopping_cart_to_navigation_home)
    }

    override fun onPlusClick(cartItem: Cart?) {
        getCartitems()
    }

    override fun onMinusClick(cartItem: Cart?) {
        getCartitems()
    }

    override fun onDeleteClick(cartItem: Cart?) {
        getCartitems()
    }

    companion object {
        private const val TAG = "CartFragment"
    }

    override fun updateMonthOnScroll(selectedDate: DayDateMonthYearModel?) {
//        currentMonthTextView.text = ""+ selectedDate?.month + " " + selectedDate?.year
//        showToast(""+ selectedDate?.month + " " + selectedDate?.year)
        mViewBinding.tvMonth.text = "" + selectedDate?.month + " " + selectedDate?.year

    }

    override fun newDateSelected(selectedDate: DayDateMonthYearModel?) {


         datefFinal = selectedDate?.year + "-" +
                selectedDate?.monthNumeric + "-" + selectedDate?.date
//        Toast.makeText(this, datefFinal, Toast.LENGTH_LONG).show()

    }

    private fun callCheckOutApi() {
        val jsonObject = JsonObject()
        jsonObject.addProperty("user_id", Utilities.getString(this, Utilities.USER_ID))
        jsonObject.addProperty("date", datefFinal)
        jsonObject.addProperty("time", formated_time)
        jsonObject.addProperty("selected_address", 1)
        val array = JsonArray()
        for (i in itemList!!.indices) {
            service_id = itemList!!.get(i).id
            quantity = itemList!!.get(i).quantity.toInt()
//            price = itemList.get(i).price.toFloat()
//            sellerId = itemList.get(i).store_id
            val `object` = JsonObject()
            `object`.addProperty("service_id", service_id)
            `object`.addProperty("qty", quantity)
            array.add(`object`)
        }
        jsonObject.add("items", array)
        val hud = KProgressHUD.create(this)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel("Please wait")
            .setCancellable(true)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)
            .show()

//        val service: APIService = RetrofitClient.getRetrofitInstance()!!.create(
//            APIService::class.java
//        )
//        val call: Call<CheckOutResponseModel> = service.checkout(jsonObject)

        apiClient.getApiService().checkout(jsonObject)
            .enqueue(object : Callback<CommonResponse?> {
                override fun onResponse(
                    call: Call<CommonResponse?>,
                    response: Response<CommonResponse?>,
                ) {
//                    assert(response.body() != null)
                    val status: Boolean = response.body()!!.statusCode
                    if (status == true) {
                        hud.dismiss()
                        Toast.makeText(
                            this@PlaceMultipleOrderActivity,
                            response.body()!!.message,
                            Toast.LENGTH_SHORT
                        ).show()
                        myDatabase?.cartDao()?.deleteAll()
                        Utilities.saveString(this@PlaceMultipleOrderActivity, "orderPlaced", "yes")
                        val intent =
                            Intent(this@PlaceMultipleOrderActivity, HomeActivity::class.java)
                        startActivity(intent)

                    } else {
                        hud.dismiss()
                        Toast.makeText(
                            this@PlaceMultipleOrderActivity,
                            "Failed" + response.body()!!.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

                override fun onFailure(call: Call<CommonResponse?>, t: Throwable) {
                    hud.dismiss()
                    t.printStackTrace()
                }
            })
    }
}