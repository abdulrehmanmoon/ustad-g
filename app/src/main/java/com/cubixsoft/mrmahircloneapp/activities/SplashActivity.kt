package com.cubixsoft.mrmahircloneapp.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.airbnb.lottie.LottieAnimationView
import com.cubixsoft.mrmahircloneapp.R

class SplashActivity : AppCompatActivity() {
    lateinit var loading_image: ImageView
    lateinit var animationView: LottieAnimationView
    lateinit var animation: Animation
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        loading_image = findViewById(R.id.loading_image)
        animationView = findViewById(R.id.animationView)
        animation = AnimationUtils.loadAnimation(this, R.anim.bounce)

        loading_image.startAnimation(animation)

        val SPLASH_DISPLAY_LENGTH = 2000
        Handler().postDelayed({ /* Create an Intent that will start the Menu-Activity. */
            val mainIntent = Intent(this, LandingActivity::class.java)
            startActivity(mainIntent)
            finish()
        }, SPLASH_DISPLAY_LENGTH.toLong())
    }
}