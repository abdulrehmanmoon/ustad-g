package com.cubixsoft.mrmahircloneapp.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.cubixsoft.mrmahircloneapp.MainActivity
import com.cubixsoft.mrmahircloneapp.Utilities
import com.cubixsoft.mrmahircloneapp.base.BaseActivityWithoutVM
import com.cubixsoft.mrmahircloneapp.databinding.ActivityLogInBinding
import com.cubixsoft.mrmahircloneapp.models.UserResponse
import com.cubixsoft.mrmahircloneapp.services.ApiClient
import com.kaopiz.kprogresshud.KProgressHUD
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LogInActivity : BaseActivityWithoutVM<ActivityLogInBinding>() {

    private lateinit var apiClient: ApiClient
    private lateinit var newToken: String
    lateinit var loginStatus: String
    override fun getViewBinding(): ActivityLogInBinding =
        ActivityLogInBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)

        apiClient = ApiClient()

        mViewBinding.apply {
            tvSignup.setOnClickListener {

                val intent = Intent(this@LogInActivity, SignupActivity::class.java)
                startActivity(intent)
            }

//            tvForgotPassword.setOnClickListener {
//
//                val intent = Intent(this@LogInActivity, ForgotPassActivity::class.java)
//                startActivity(intent)
//            }
            tvSignup1.setOnClickListener {

                val intent = Intent(this@LogInActivity, SignupActivity::class.java)
                startActivity(intent)
            }
            btnLogIn.setOnClickListener {

                val getEmail: String = etEmailId.text.toString()
                val getPass: String = etPasswordId.text.toString()
                if (!getEmail.equals("")) {
                    if (!getPass.equals("")) {
                        loginApi(getEmail, getPass)

                    } else {
                        showToast("Enter Password")
                    }
                } else {
                    showToast("Enter Email Address")

                }
            }
        }
    }

    fun loginApi(email: String, pass: String) {
        val hud = KProgressHUD.create(this)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel("Please wait")
            .setCancellable(true)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)
            .show()

        apiClient.getApiService().login(email, pass)
            .enqueue(object : Callback<UserResponse> {
                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                    hud.dismiss()
                    Toast.makeText(applicationContext, "false" + t.message, Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onResponse(
                    call: Call<UserResponse>,
                    response: Response<UserResponse>,
                ) {
                    val loginResponse = response.body()
                    if (loginResponse!!.status==true) {
                        hud.dismiss()
                        showToast(loginResponse.message)
                        val intent = Intent(this@LogInActivity, MainActivity::class.java)
                        Utilities.saveString(
                            this@LogInActivity,
                            Utilities.USER_ID,
                            loginResponse.user.id.toString()
                        )
                        Utilities.saveString(this@LogInActivity, "loginStatus", "yes")
                        Utilities.saveString(
                            this@LogInActivity,
                            Utilities.USER_FNAME,
                            loginResponse.user.name
                        )

                        Utilities.saveString(
                            this@LogInActivity,
                            Utilities.USER_EMAIL,
                            loginResponse.user.email
                        )

                        Utilities.saveString(
                            this@LogInActivity,
                            Utilities.USER_PROFILE_IMAGE,
                            loginResponse.user.profile_image
                        )
                        startActivity(intent)
                        finish()


                    } else {
//                        val loginResponse = response.body()
                        hud.dismiss()
                        if (loginResponse != null) {
                            showToast(loginResponse.message)
                        }
                    }
                }
            })
    }
}