package com.cubixsoft.mrmahircloneapp

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.cubixsoft.mrmahircloneapp.activities.NotificationActivity
import com.cubixsoft.mrmahircloneapp.activities.PlaceMultipleOrderActivity
import com.cubixsoft.mrmahircloneapp.activities.SplashActivity
import com.cubixsoft.mrmahircloneapp.base.BaseActivityWithoutVM
import com.cubixsoft.mrmahircloneapp.databinding.ActivityHomeBinding
import com.cubixsoft.mrmahircloneapp.services.Constants
import com.squareup.picasso.Picasso
import libs.mjn.prettydialog.BuildConfig
import libs.mjn.prettydialog.PrettyDialog
import libs.mjn.prettydialog.PrettyDialogCallback

class HomeActivity : BaseActivityWithoutVM<ActivityHomeBinding>() {
    private val END_SCALE = 0.7f
    lateinit var navController: NavController
    private val RC_LOCATION_REQUEST = 1234
    var isChange = false
    lateinit var profileIc: ImageView
    lateinit var hView: View
    lateinit var email:TextView
    lateinit var textView_header:TextView
    override fun getViewBinding(): ActivityHomeBinding =
        ActivityHomeBinding.inflate(layoutInflater)

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    @SuppressLint("UseCompatLoadingForDrawables")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)
        val orderPlaced: String = Utilities.getString(this, "orderPlaced")
        navController = Navigation.findNavController(this, R.id.fragment)
        initNavigation()
        if (orderPlaced.equals("yes")) {
            navController.navigate(R.id.myOrderFragment);
        }
        mViewBinding.apply {
            val userName: String = Utilities.getString(this@HomeActivity, Utilities.USER_FNAME)
            val userEmail: String = Utilities.getString(this@HomeActivity, Utilities.USER_EMAIL)
            hView = navigationView.getHeaderView(0)
             textView_header = hView.findViewById<TextView>(R.id.user_name)
             profileIc = hView.findViewById<ImageView>(R.id.profileIc)
             email = hView.findViewById<TextView>(R.id.email)
            textView_header.setText(userName)
            email.setText(userEmail)
            val profileImage: String = Utilities.getString(this@HomeActivity, Utilities.USER_PROFILE_IMAGE)
            if (!TextUtils.isEmpty(profileImage)) {

                Picasso.get()
                    .load(Constants.BASE_URL_IMG + profileImage).placeholder(R.drawable.service_ic)
                    .into(profileIc)

            } else {
                Picasso.get()
                    .load(R.drawable.service_ic).placeholder(R.drawable.service_ic)
                    .into(profileIc)

            }
            ivNotifications.setOnClickListener {
                startActivity(Intent(this@HomeActivity, NotificationActivity::class.java))
            }
//            navView.setupWithNavController(navController)
            bottomNavigation.setupWithNavController(navController)
            ivCart.setOnClickListener {
                startActivity(Intent(this@HomeActivity, PlaceMultipleOrderActivity::class.java))
            }

            val mDrawerToggle: ActionBarDrawerToggle = object : ActionBarDrawerToggle(
                this@HomeActivity,
                drawerLayout,
                R.string.open,
                R.string.closed
            ) {
                override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                    // Scale the View based on current slide offset
                    val diffScaledOffset: Float = slideOffset * (1 - END_SCALE)
                    val offsetScale = 1 - diffScaledOffset
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        mainContainer.setScaleX(offsetScale)
                        mainContainer.setScaleY(offsetScale)
                    }


                    // Translate the View, accounting for the scaled width
                    val xOffset = drawerView.width * slideOffset
                    val yOffset = drawerView.height * slideOffset
                    val xOffsetDiff: Float = mainContainer.getWidth() * diffScaledOffset / 2
                    val xTranslation = xOffset - xOffsetDiff
                    val yOffsetDiff: Float = mainContainer.getHeight() * diffScaledOffset / 15
                    val yTranslation = yOffset - yOffsetDiff
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        mainContainer.setTranslationX(xTranslation)
                    }
                }
            }

            drawerLayout.addDrawerListener(mDrawerToggle)

            drawerLayout.setScrimColor(resources.getColor(android.R.color.transparent))

            /*Remove navigation drawer shadow/fadding*/

            /*Remove navigation drawer shadow/fadding*/drawerLayout.setDrawerElevation(0f)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                drawerLayout.setElevation(0f)
                mainContainer.setElevation(10.0f)
            }

            initNavigation()

        }

    }

    private fun initNavigation() {
        mViewBinding.ivMenu.setOnClickListener(View.OnClickListener {
            mViewBinding.drawerLayout.openDrawer(GravityCompat.START,
                true)
        })

        NavigationUI.setupWithNavController(mViewBinding.navigationView, navController)
        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            if (destination.label != null) {
                if (destination.label == "Dashboard") {
                    mViewBinding.rlTop.setVisibility(View.VISIBLE)
                } else {
                    mViewBinding.rlTop.setVisibility(View.GONE)
                    //                        tvTitle.setText(destination.getLabel());
                }
            }
        }

        mViewBinding.navigationView.getMenu().findItem(R.id.notifcations)
            .setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener {
                startActivity(Intent(this, NotificationActivity::class.java))
                mViewBinding.drawerLayout.closeDrawer(GravityCompat.START, false)
                true
            })

        mViewBinding.navigationView.getMenu().findItem(R.id.logout)
            .setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener {
                showCustomDialog()
                mViewBinding.drawerLayout.closeDrawer(GravityCompat.START, false)
                true
            })
        mViewBinding.navigationView.getMenu().findItem(R.id.shareApp)
            .setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener {
                try {
                    val sendIntent = Intent()
                    sendIntent.action = Intent.ACTION_SEND
                    sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Hey check out my app at: https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)
                    sendIntent.type = "text/plain"
                    startActivity(sendIntent)
                } catch (e: Exception) {
                    //e.toString();
                }
                mViewBinding.drawerLayout.closeDrawer(GravityCompat.START, false)
                true
            })
        mViewBinding.navigationView.getMenu().findItem(R.id.rateus)
            .setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse("market://details?id=com.android.app")
                try {
                    startActivity(intent)
                } catch (e: Exception) {
                    intent.data =
                        Uri.parse("https://play.google.com/store/apps/details?id=com.android.app")
                }
                mViewBinding.drawerLayout.closeDrawer(GravityCompat.START, false)
                true
            })
//        val item: MenuItem = mViewBinding.navigationView.getMenu().findItem(R.id.logout)
//        if (Utilities.getString(this, "user_loginfor_logout").equals("yes")) {
//            mViewBinding.navigationView.getMenu().findItem(R.id.logout)
//                .setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener { item ->
//                    item.isVisible = true
//                    showCustomDialog()
//                    drawer.closeDrawer(GravityCompat.START, false)
//                    true
//                })
//        } else {
//            item.isVisible = false
//        }

//
    }

    private fun showCustomDialogforExit() {
        val pDialog = PrettyDialog(this)
        pDialog
            .setTitle("Message")
            .setMessage("Are you sure you want to Exit?")
            .setIcon(R.drawable.pdlg_icon_info)
            .setIconTint(R.color.colorPrimary)
            .addButton(
                "Yes",
                R.color.colorPrimary,
                R.color.pdlg_color_white,
                object : PrettyDialogCallback {
                    override fun onClick() {
                        pDialog.dismiss()
                        finish()
                        finishAffinity()
                    }
                })
            .addButton("No",
                R.color.pdlg_color_red,
                R.color.pdlg_color_white,
                object : PrettyDialogCallback {
                    override fun onClick() {
                        pDialog.dismiss()
                    }
                })
            .show()
    }


    override fun onBackPressed() {
//            super.onBackPressed()
        if (navController.currentDestination
                ?.getLabel().toString() != "Home"
        ) {
            super.onBackPressed()
        } else {
            showCustomDialogforExit()
        }
    }

    private fun showCustomDialog() {
        val pDialog = PrettyDialog(this)
        pDialog
            .setTitle("Message")
            .setMessage("Are you sure you want to Logout?")
            .setIcon(R.drawable.pdlg_icon_info)
            .setIconTint(R.color.color_primary)
            .addButton(
                "Yes",
                R.color.pdlg_color_white,
                R.color.color_primary
            ) {
                Utilities.clearSharedPref(this)
                startActivity(Intent(this, SplashActivity::class.java))
                finish()
                pDialog.dismiss()
            }
            .addButton(
                "No",
                R.color.pdlg_color_white,
                R.color.pdlg_color_red
            ) { pDialog.dismiss() }
            .show()
    }
    override fun onResume() {
        super.onResume()
        val profileImage: String =
            Utilities.getString(this, Utilities.USER_PROFILE_IMAGE)
        val userName: String = Utilities.getString(this@HomeActivity, Utilities.USER_FNAME)
        val userEmail: String = Utilities.getString(this@HomeActivity, Utilities.USER_EMAIL)
        email.setText(userEmail)
        textView_header.setText(userName)
        if (!TextUtils.isEmpty(profileImage)) {

            Picasso.get()
                .load(Constants.BASE_URL_IMG + profileImage)
                .into(profileIc)

        } else {
            Picasso.get()
                .load(R.drawable.profile_ic).placeholder(R.drawable.profile_ic)
                .into(profileIc)

        }
    }
}