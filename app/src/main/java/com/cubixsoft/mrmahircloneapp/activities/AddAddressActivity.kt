package com.cubixsoft.mrmahircloneapp.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.cubixsoft.mrmahircloneapp.MainActivity
import com.cubixsoft.mrmahircloneapp.Utilities
import com.cubixsoft.mrmahircloneapp.adapters.MyInfoWindowAdapter
import com.cubixsoft.mrmahircloneapp.base.BaseActivityWithoutVM
import com.cubixsoft.mrmahircloneapp.databinding.ActivityAddressBinding
import com.cubixsoft.mrmahircloneapp.models.CommonResponse
import com.cubixsoft.mrmahircloneapp.models.CommonResponseInt
import com.cubixsoft.mrmahircloneapp.models.UserResponse
import com.cubixsoft.mrmahircloneapp.services.ApiClient
import com.google.android.gms.common.api.Status
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.android.material.snackbar.Snackbar
import com.kaopiz.kprogresshud.KProgressHUD
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class AddAddressActivity : BaseActivityWithoutVM<ActivityAddressBinding>() {
    private lateinit var apiClient: ApiClient
    private var autocompleteFragment: AutocompleteSupportFragment? =
        null
    private var myGoogleMap: GoogleMap? = null
    private var selectedLatLng: LatLng? = null
    var pickupLatitude: String = ""
    var pickupLongitude: String = ""
    var getPickupAddress: String = ""
    var address: String = ""
    var getPlaceName: String = ""
    var AUTOCOMPLETE_REQUEST_CODE: Int = 1

    override fun getViewBinding(): ActivityAddressBinding =
        ActivityAddressBinding.inflate(layoutInflater)

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    @SuppressLint("UseCompatLoadingForDrawables")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)
        mViewBinding.btnAdd.setOnClickListener {
            Utilities.saveString(this, "savedAddress", "yes")
        }
        val apiKey = getString(com.cubixsoft.mrmahircloneapp.R.string.api_key)

        /**
         * Initialize Places. For simplicity, the API key is hard-coded. In a production
         * environment we recommend using a secure mechanism to manage API keys.
         */
        /**
         * Initialize Places. For simplicity, the API key is hard-coded. In a production
         * environment we recommend using a secure mechanism to manage API keys.
         */
        if (!Places.isInitialized()) {
            Places.initialize(applicationContext, apiKey)
        }
        val autocompleteFragment =
            supportFragmentManager.findFragmentById(com.cubixsoft.mrmahircloneapp.R.id.autocomplete_fragment) as AutocompleteSupportFragment?

        autocompleteFragment!!.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME))
        val placesClient = Places.createClient(this)
//        val fields = ArrayList<Place.Field>()
//        fields.add(Place.Field.NAME)
//        fields.add(Place.Field.ADDRESS)
//        fields.add(Place.Field.LAT_LNG)
//        startActivityForResult(Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)
//            .build(this), AUTOCOMPLETE_REQUEST_CODE);
        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                // TODO: Get info about the selected place.
                selectedLatLng = place.latLng
                getPlaceName = place.getName()
                showToast("Place: " + place.getName() + ", " + place.getId())
                if (selectedLatLng != null) {
                    pickupLatitude = selectedLatLng!!.latitude.toString()
                    pickupLongitude = selectedLatLng!!.longitude.toString()
                    getPickupAddress = Utilities.getAddress(applicationContext, selectedLatLng)
                    showToast("Clicked" + pickupLatitude + pickupLongitude + getPickupAddress)
                    showToast("Place: " + place.getName() + ", " + place.getId())
                    Utilities.saveString(this@AddAddressActivity, "getAddress", address)


                }
            }

            override fun onError(status: Status) {
                // TODO: Handle the error.
                Log.i("YAM", "An error occurred: $status")
            }
        })


        mViewBinding.ivBack.setOnClickListener {

            finish()
        }
        mViewBinding.btnAdd.setOnClickListener {

            val getTitle: String = mViewBinding.etAddresTitle.text.toString()
            if (!getTitle.equals("")) {
                if (!getPlaceName.equals("")) {
                    addAddressApi(Utilities.getString(this,Utilities.USER_ID),getTitle,getPlaceName,"0.333","0.454")

                } else {
                    showToast("Select Place")
                }
            } else {
                showToast("Enter Address Title")

            }
        }
    }


    fun addAddressApi(userId: String, title: String, address: String, lat: String, long: String) {
        val hud = KProgressHUD.create(this)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel("Please wait")
            .setCancellable(true)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)
            .show()
    apiClient=ApiClient()

        apiClient.getApiService().add_address(userId, title, address, lat, long)
            .enqueue(object : Callback<CommonResponseInt> {
                override fun onFailure(call: Call<CommonResponseInt>, t: Throwable) {
                    hud.dismiss()
                    Toast.makeText(applicationContext, "false" + t.message, Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onResponse(
                    call: Call<CommonResponseInt>,
                    response: Response<CommonResponseInt>,
                ) {
                    val loginResponse = response.body()
                    if (loginResponse!!.statusCode == 200) {
                        hud.dismiss()
                        showToast(loginResponse.message)
                        val intent = Intent(this@AddAddressActivity, MainActivity::class.java)

                        startActivity(intent)
                        finish()


                    } else {
//                        val loginResponse = response.body()
                        hud.dismiss()
                        if (loginResponse != null) {
                            showToast(loginResponse.message)
                        }
                    }
                }
            })
    }

    //
//    override fun onMapReady(googleMap: GoogleMap) {
//        myGoogleMap = googleMap
//
//        //Setting the Custom Marker Title
//        myGoogleMap!!.setInfoWindowAdapter(MyInfoWindowAdapter(applicationContext))
//
//        //First show the Selected Location that is stored in the SharedPref, If no location is saved
//        //then show the current location
//        // showSavedLatLng returns TRUE if the savedLocation is successfully shown on map otherwise FALSE
//
//
//        // Set up a PlaceSelectionListener to handle the response.
//        autocompleteFragment!!.setOnPlaceSelectedListener(object : PlaceSelectionListener {
//            private val place: Place? = null
//            override fun onPlaceSelected(place: Place) {
//                // TODO: Get info about the selected place.
//                selectedLatLng = place.latLng
//                if (selectedLatLng != null) {
//                    pickupLatitude = selectedLatLng!!.latitude.toString()
//                    pickupLongitude = selectedLatLng!!.longitude.toString()
//                    getPickupAddress = Utilities.getAddress(applicationContext, selectedLatLng)
//                    Snackbar.make(
//                        mViewBinding.root,
//                        "Clicked" + pickupLatitude + pickupLongitude + getPickupAddress,
//                        Snackbar.LENGTH_LONG
//                    ).show()
//                    showToast(pickupLatitude + pickupLongitude)
//                    Utilities.saveString(this@AddAddressActivity, "getAddress", address)
//                    showToast(address)
//                }
//            }
//
//            override fun onError(status: Status) {
//                // TODO: Handle the error.
//                Log.i("YAM", "An error occurred: $status")
//            }
//        })
//    }
}