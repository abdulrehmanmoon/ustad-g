package com.cubixsoft.mrmahircloneapp.utils

import com.cubixsoft.mrmahircloneapp.roomDbs.Cart
import com.myapps.healthapp1.models.subCategories.ServicesDataModel

interface CartServicesClickListner {
    fun onPlusClick(cartItem: ServicesDataModel?,position:Int)
    fun onMinusClick(cartItem: ServicesDataModel?,position:Int)
    fun onDeleteClick(cartItem: ServicesDataModel?)
}
