package com.cubixsoft.mrmahircloneapp.utils

import com.cubixsoft.mrmahircloneapp.roomDbs.Cart

interface CartClickListner {
    fun onPlusClick(cartItem: Cart?)
    fun onMinusClick(cartItem: Cart?)
    fun onDeleteClick(cartItem: Cart?)
}
