package com.cubixsoft.mrmahircloneapp.fragments.orders

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.cubixsoft.mrmahircloneapp.HomeActivity
import com.cubixsoft.mrmahircloneapp.Utilities
import com.cubixsoft.mrmahircloneapp.adapters.PastOrderAdapter
import com.cubixsoft.mrmahircloneapp.databinding.FragmentPastorderBinding
import com.cubixsoft.mrmahircloneapp.services.ApiClient
import com.kaopiz.kprogresshud.KProgressHUD
import com.myapps.healthapp1.models.subCategories.MainOrderResponse
import com.myapps.healthapp1.models.subCategories.PastOrderDataModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CurrentOrderFragment : Fragment() {

    private lateinit var binding: FragmentPastorderBinding
    private lateinit var apiClient: ApiClient
    var categoryId: String = ""
    var userId: String = ""
    lateinit var title: String
    var list: List<PastOrderDataModel>? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {

        binding = FragmentPastorderBinding.inflate(inflater, container, false)
        categoryId = Utilities.getString(activity, "categoryId")
        userId = Utilities.getString(requireContext(),Utilities.USER_ID)
        title = Utilities.getString(activity, "title")
        apiClient = ApiClient()
        getSubCategoryApi()
        return binding.root

    }

    fun getSubCategoryApi() {
        val hud = KProgressHUD.create(activity)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel("Please wait")
            .setCancellable(true)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)
            .show()

        apiClient.getApiService().getPastorderList(userId, "current")
            .enqueue(object : Callback<MainOrderResponse> {

                override fun onFailure(call: Call<MainOrderResponse>, t: Throwable) {
                    hud.dismiss()
                    Toast.makeText(
                        activity?.applicationContext,
                        t.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()

                    binding.tvStatus.visibility = View.VISIBLE
                }

                override fun onResponse(
                    call: Call<MainOrderResponse>,
                    response: Response<MainOrderResponse>,
                ) {
                    val loginResponse = response.body()
                    if (loginResponse?.statusCode == true) {
                        list = response.body()!!.getData
                        if ( list == null || list.isNullOrEmpty()){
                            binding.tvStatus.visibility = View.VISIBLE
                            Toast.makeText(
                                activity?.applicationContext,
                                "Data Not Found",
                                Toast.LENGTH_SHORT
                            )
                                .show()

                        }else{
                            binding.tvStatus.visibility = View.GONE

                        }
                        hud.dismiss()
                        Toast.makeText(
                            activity?.applicationContext,
                            loginResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()
                        binding.recyclerview.apply {
                            setHasFixedSize(true)
                            layoutManager =
                                GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false)
//                            adapter = activity?.let { SuggestedAdapterNew(sugestedmodel, it) };
                            adapter = PastOrderAdapter(
                                context as HomeActivity, list
                            )


                        }

                    } else {
//                        val loginResponse = response.body()
                        hud.dismiss()
                        if (loginResponse != null) {
                            Toast.makeText(
                                activity?.applicationContext,
                                loginResponse.message,
                                Toast.LENGTH_SHORT
                            ).show()
                            binding.tvStatus.visibility = View.VISIBLE

                        }
                    }
                }

            })
    }
}