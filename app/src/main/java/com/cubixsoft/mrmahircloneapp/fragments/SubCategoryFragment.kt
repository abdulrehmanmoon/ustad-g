package com.cubixsoft.mrmahircloneapp.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.cubixsoft.mrmahircloneapp.HomeActivity
import com.cubixsoft.mrmahircloneapp.Utilities
import com.cubixsoft.mrmahircloneapp.adapters.SubCategoryAdapter
import com.cubixsoft.mrmahircloneapp.databinding.FragmentCategoryBinding
import com.cubixsoft.mrmahircloneapp.services.ApiClient
import com.kaopiz.kprogresshud.KProgressHUD
import com.myapps.healthapp1.models.subCategories.MainSubCategResponse
import com.myapps.healthapp1.models.subCategories.SubCategoryDataModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SubCategoryFragment : Fragment() {
    private lateinit var binding: FragmentCategoryBinding
    private lateinit var apiClient: ApiClient
     var categoryId:String=""
    lateinit var title:String
    lateinit var cityName:String
    var list: List<SubCategoryDataModel>? = null
    @SuppressLint("UseRequireInsteadOfGet")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCategoryBinding.inflate(inflater, container, false)
        apiClient = ApiClient()
        categoryId= Utilities.getString(activity,"categoryId")
        cityName= Utilities.getString(requireContext(),"userCityName")
        title=Utilities.getString(activity,"title")
        getSubCategoryApi()

        binding.ivBack.setOnClickListener {
            activity?.onBackPressed()
        }
        binding.tvTitle.setText(title)
        return binding.root

    }

    fun getSubCategoryApi() {
        binding.shimmerViewContainer.startShimmerAnimation()

        apiClient.getApiService().getSubCategoryApi(categoryId,cityName)
            .enqueue(object : Callback<MainSubCategResponse> {

                override fun onFailure(call: Call<MainSubCategResponse>, t: Throwable) {
                    Toast.makeText(
                        activity?.applicationContext,
                         t.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()

                    binding.tvStatus.visibility = View.VISIBLE
//                    binding.shimmerViewContainer.visibility = View.GONE
                }

                override fun onResponse(
                    call: Call<MainSubCategResponse>,
                    response: Response<MainSubCategResponse>
                ) {
                    val loginResponse = response.body()
                    if (loginResponse?.statusCode == true) {
                        list = response.body()!!.getData

                        Toast.makeText(
                            activity?.applicationContext,
                            loginResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()
                        binding.recyclerview.apply {
                            setHasFixedSize(true)
                            layoutManager =
                                GridLayoutManager(activity,2, GridLayoutManager.VERTICAL, false)
//                            adapter = activity?.let { SuggestedAdapterNew(sugestedmodel, it) };
                            adapter = SubCategoryAdapter(
                                context as HomeActivity, list
                            )
                            binding.shimmerViewContainer.stopShimmerAnimation()
                            binding.tvStatus.visibility = View.GONE
                            binding.shimmerViewContainer.visibility = View.GONE

                        }
                    } else {
//                        val loginResponse = response.body()

                        if (loginResponse != null) {
                            Toast.makeText(
                                activity?.applicationContext,
                                 loginResponse.message,
                                Toast.LENGTH_SHORT
                            ).show()
                            binding.tvStatus.visibility = View.VISIBLE
//                            binding.shimmerViewContainer.visibility = View.GONE

                        }
                    }
                }

            })
    }
}