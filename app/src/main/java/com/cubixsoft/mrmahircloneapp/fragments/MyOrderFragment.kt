package com.cubixsoft.mrmahircloneapp.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import com.cubixsoft.mrmahircloneapp.R
import com.google.android.material.tabs.TabLayoutMediator.TabConfigurationStrategy
import com.cubixsoft.mrmahircloneapp.Utilities
import com.cubixsoft.mrmahircloneapp.adapters.ImageSliderAdapter
import com.cubixsoft.mrmahircloneapp.databinding.FragmentMyOrderBinding
import com.cubixsoft.mrmahircloneapp.fragments.orders.CurrentOrderFragment
import com.cubixsoft.mrmahircloneapp.fragments.orders.PastOrderFragment
import com.cubixsoft.mrmahircloneapp.models.slider.MainSliderResponse
import com.cubixsoft.mrmahircloneapp.services.ApiClient
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.kaopiz.kprogresshud.KProgressHUD
import com.myapps.healthapp1.models.subCategories.SubCategoryDataModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class MyOrderFragment : Fragment() {
    private lateinit var binding: FragmentMyOrderBinding
    private lateinit var apiClient: ApiClient
    var categoryId: String = ""
    lateinit var title: String
    private lateinit var adapter: ViewPagerAdapter
    var fragment: Fragment? = null
    var fragmentTransaction: FragmentTransaction? = null
    var list: List<SubCategoryDataModel>? = null

    @SuppressLint("UseRequireInsteadOfGet")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentMyOrderBinding.inflate(inflater, container, false)
        apiClient = ApiClient()
        categoryId = Utilities.getString(activity, "categoryId")
        Utilities.saveString(requireContext(),"orderPlaced","no")
        title = Utilities.getString(activity, "title")
//        getSubCategoryApi()

        binding.ivBack.setOnClickListener {
            activity?.onBackPressed()
        }
        initView()

        return binding.root

    }

    fun showToast(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    private fun initView() {
        setupViewPager(binding.viewPager)
        //        binding.tabLayout.setupWithViewPager(binding.viewPager);
        TabLayoutMediator(binding.tabLayout, binding.viewPager,
            TabConfigurationStrategy { tab: TabLayout.Tab, position: Int ->
                tab.text = adapter.mFragmentTitleList.get(
                    position
                )
            }
        ).attach()
        for (i in 0 until binding.tabLayout.tabCount) {
            val tv = LayoutInflater.from(activity)
                .inflate(R.layout.custom_tab, null) as TextView
            binding.tabLayout.getTabAt(i)!!.customView = tv
        }
    }

    private fun setupViewPager(viewPager: ViewPager2) {
        adapter = ViewPagerAdapter(
            requireActivity().supportFragmentManager,
            requireActivity().lifecycle
        )
        adapter.addFragment(CurrentOrderFragment(), "Upcomming")
        adapter.addFragment(PastOrderFragment(), "Ongoing")
        adapter.addFragment(PastOrderFragment(), "Previous")
        viewPager.adapter = adapter
        viewPager.offscreenPageLimit = 1
    }


    internal class ViewPagerAdapter(
        fragmentManager: FragmentManager,
        lifecycle: Lifecycle,
    ) :
        FragmentStateAdapter(fragmentManager, lifecycle) {
        private val mFragmentList: MutableList<Fragment> =
            ArrayList()
        val mFragmentTitleList: MutableList<String> =
            ArrayList()

        fun addFragment(
            fragment: Fragment,
            title: String,
        ) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun createFragment(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getItemCount(): Int {
            return mFragmentList.size
        }
    }
}