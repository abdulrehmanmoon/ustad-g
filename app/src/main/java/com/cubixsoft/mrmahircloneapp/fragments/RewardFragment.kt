package com.cubixsoft.mrmahircloneapp.fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import com.cubixsoft.mehrancashcarry.models.HomePagerModel
import com.cubixsoft.mrmahircloneapp.R
import com.cubixsoft.mrmahircloneapp.Utilities
import com.cubixsoft.mrmahircloneapp.databinding.FragmentRewardBinding
import com.cubixsoft.mrmahircloneapp.services.ApiClient
import com.google.android.material.bottomsheet.BottomSheetDialog
import java.util.*

class RewardFragment : Fragment() {
    private lateinit var binding: FragmentRewardBinding
    private lateinit var apiClient: ApiClient
    var list: ArrayList<HomePagerModel>? = null

    @SuppressLint("UseRequireInsteadOfGet", "ResourceAsColor")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentRewardBinding.inflate(inflater, container, false)
        apiClient = ApiClient()
        val points: String = Utilities.getString(requireContext(), Utilities.USER_Points)
        binding.tvpoints.setText(points)
        onClicks()
        return binding.root
    }

    private fun onClicks() {
        binding.layoutFor1KPoint.setOnClickListener {
            showDialodPimpMode(requireActivity(), "1000")
        }

        binding.ivBack.setOnClickListener {
            requireActivity().onBackPressed()
        }
        binding.layoutFor25KPoint.setOnClickListener {
            showDialodPimpMode(requireActivity(), "2500")
        }
        binding.layoutFor5KPoint.setOnClickListener {
            showDialodPimpMode(requireActivity(), "5000")
        }

    }

    private fun showDialodPimpMode(activity: Activity, str: String) {
        val bottomSheetDialog = BottomSheetDialog(activity)
        bottomSheetDialog.setContentView(R.layout.get_reward_dialog)
        val tvPointss = bottomSheetDialog.findViewById<TextView>(R.id.tvPointss)
        val bDone = bottomSheetDialog.findViewById<AppCompatButton>(R.id.bDone)
        tvPointss!!.text =
            "You need " + str + " points to redeem this credit. Take more order to earn more points."
        bDone!!.setOnClickListener {
            bottomSheetDialog.dismiss()
        }
        bottomSheetDialog.show()
    }
}