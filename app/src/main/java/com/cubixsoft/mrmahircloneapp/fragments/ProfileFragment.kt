package com.cubixsoft.mrmahircloneapp.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.cubixsoft.mehrancashcarry.models.HomePagerModel
import com.cubixsoft.mrmahircloneapp.R
import com.cubixsoft.mrmahircloneapp.Utilities
import com.cubixsoft.mrmahircloneapp.activities.EditProfileActivity
import com.cubixsoft.mrmahircloneapp.activities.SplashActivity
import com.cubixsoft.mrmahircloneapp.databinding.FragmentProfileBinding
import com.cubixsoft.mrmahircloneapp.services.ApiClient
import com.cubixsoft.mrmahircloneapp.services.Constants
import com.squareup.picasso.Picasso
import libs.mjn.prettydialog.BuildConfig
import libs.mjn.prettydialog.PrettyDialog
import java.io.File
import java.util.*

class ProfileFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {
    private lateinit var binding: FragmentProfileBinding
    private lateinit var apiClient: ApiClient
    var list: ArrayList<HomePagerModel>? = null
    var input: String = ""
    var uri1: Uri? = null
    var image: File? = null
    var selected_img_bitmap: Bitmap? = null
    lateinit var ivImage: ImageView

    @SuppressLint("UseRequireInsteadOfGet", "ResourceAsColor")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentProfileBinding.inflate(inflater, container, false)
        apiClient = ApiClient()
        val profileImage: String =
            Utilities.getString(requireContext(), Utilities.USER_PROFILE_IMAGE)
        val userName: String = Utilities.getString(requireContext(), Utilities.USER_FNAME)
        val userEmail: String = Utilities.getString(requireContext(), Utilities.USER_EMAIL)
        if (!TextUtils.isEmpty(userName)||!TextUtils.isEmpty(userEmail)) {
            binding.tvName.text = userName
            binding.tvEmail.text = userEmail
        }else{
            binding.tvName.text = "Name"
            binding.tvEmail.text = "Email"
        }
        if (!TextUtils.isEmpty(profileImage)) {

            Picasso.get()
                .load(Constants.BASE_URL_IMG + profileImage).placeholder(R.drawable.profile_ic)
                .into(binding.profileImage)

        } else {
            Picasso.get()
                .load(R.drawable.profile_ic).placeholder(R.drawable.profile_ic)
                .into(binding.profileImage)

        }
        onClicks()
        return binding.root
    }

    private fun onClicks() {
        binding.ivBack.setOnClickListener {
            requireActivity().onBackPressed()
        }
        binding.editprofile.setOnClickListener {

            val intent = Intent(getActivity(), EditProfileActivity::class.java)
            getActivity()?.startActivity(intent)
        }

        binding.llLogout.setOnClickListener {

            showCustomDialogforExit()
        }

        binding.shareFriend.setOnClickListener {
            try {
                val sendIntent = Intent()
                sendIntent.action = Intent.ACTION_SEND
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                    "Hey check out my app at: https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)
                sendIntent.type = "text/plain"
                startActivity(sendIntent)
            } catch (e: Exception) {
                //e.toString();
            }
        }

        binding.rlRateus.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("market://details?id=com.android.app")
            try {
                startActivity(intent)
            } catch (e: Exception) {
                intent.data =
                    Uri.parse("https://play.google.com/store/apps/details?id=com.android.app")
            }
        }


    }

    override fun onRefresh() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            fragmentManager?.beginTransaction()?.detach(this)?.commitNow();
            fragmentManager?.beginTransaction()?.attach(this)?.commitNow();
        } else {
            fragmentManager?.beginTransaction()?.detach(this)?.attach(this)?.commit();
        }
    }


    private fun showCustomDialogforExit() {
        val pDialog = PrettyDialog(activity)
        pDialog
            .setTitle("Message")
            .setMessage("Are you sure you want to Logout?")
            .setIcon(R.drawable.pdlg_icon_info)
            .setIconTint(R.color.colorPrimary)
            .addButton(
                "Yes",
                R.color.colorPrimary,
                R.color.pdlg_color_white
            ) {
                startActivity(Intent(activity, SplashActivity::class.java))
                Utilities.saveString(requireContext(), "loginStatus", "no")
                pDialog.dismiss()
            }
            .addButton(
                "No",
                R.color.pdlg_color_red,
                R.color.pdlg_color_white
            ) { pDialog.dismiss() }
            .show()

    }

    override fun onResume() {
        super.onResume()

        val profileImage: String =
            Utilities.getString(requireContext(), Utilities.USER_PROFILE_IMAGE)
        val userName: String = Utilities.getString(requireContext(), Utilities.USER_FNAME)
        val userEmail: String = Utilities.getString(requireContext(), Utilities.USER_EMAIL)
        binding.tvName.setText(userName)
        binding.tvEmail.setText(userEmail)
        if (!TextUtils.isEmpty(profileImage)) {

            Picasso.get()
                .load(Constants.BASE_URL_IMG + profileImage)
                .into(binding.profileImage)

        } else {
            Picasso.get()
                .load(R.drawable.profile_ic).placeholder(R.drawable.profile_ic)
                .into(binding.profileImage)

        }
    }
}