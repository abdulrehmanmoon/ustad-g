package com.cubixsoft.mrmahircloneapp.fragments

import android.Manifest
import android.R
import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.app.ShareCompat.getCallingActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.cubixsoft.mrmahircloneapp.HomeActivity
import com.cubixsoft.mrmahircloneapp.Utilities
import com.cubixsoft.mrmahircloneapp.activities.PlaceMultipleOrderActivity
import com.cubixsoft.mrmahircloneapp.adapters.ServicesAdapter
import com.cubixsoft.mrmahircloneapp.databinding.BottomSheetDialogBinding
import com.cubixsoft.mrmahircloneapp.databinding.FragmentServiceBinding
import com.cubixsoft.mrmahircloneapp.models.services.SearchSuggestionModel
import com.cubixsoft.mrmahircloneapp.roomDbs.Cart
import com.cubixsoft.mrmahircloneapp.roomDbs.MyDatabase
import com.cubixsoft.mrmahircloneapp.services.ApiClient
import com.cubixsoft.mrmahircloneapp.utils.CartServicesClickListner
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.kaopiz.kprogresshud.KProgressHUD
import com.myapps.healthapp1.models.subCategories.MainServiceResponse
import com.myapps.healthapp1.models.subCategories.ServicesDataModel
import com.varunjohn1990.iosdialogs4android.IOSDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ServicesFragment : Fragment(), ServicesAdapter.Callback, CartServicesClickListner {
    private lateinit var binding: FragmentServiceBinding
    private lateinit var bottomSheetDialog: BottomSheetDialogBinding
    private lateinit var apiClient: ApiClient
    var categoryId: String = ""
    lateinit var title: String
    lateinit var domainId: String
    var myDatabase: MyDatabase? = null
    var counter = "1"
    var total = 0.0
    var subCategoryId: String = ""
    var MY_PERMISSIONS_REQUEST_CALL_PHONE = 101
    private var itemList: List<Cart>? = null
    private var merk: ArrayList<SearchSuggestionModel> = ArrayList()
    var list: List<ServicesDataModel>? = null
    var selection: String = ""
    var bottomsheet: BottomSheetDialog? = null
    @SuppressLint("UseRequireInsteadOfGet")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentServiceBinding.inflate(inflater, container, false)
        bottomSheetDialog = BottomSheetDialogBinding.inflate(inflater, container, false)
        apiClient = ApiClient()
        categoryId = Utilities.getString(activity, "categoryId")
        title = Utilities.getString(activity, "title")
        subCategoryId = Utilities.getString(activity, "subCategoryId")
        myDatabase =
            activity?.let {
                Room.databaseBuilder(it,
                    MyDatabase::class.java, "My_Cart").allowMainThreadQueries().build()
            }
         domainId = Utilities.getString(activity, "domainId")
        getServicesApi()

        binding.ivBack.setOnClickListener {
            activity?.onBackPressed()
        }
        binding.tvTitle.setText(title)
        onClicks()


        return binding.root

    }

    private fun onClicks() {
        binding.etSearch.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, rowId ->
                selection = binding.etSearch.text.toString()
                selection = parent.getItemAtPosition(position) as String

            }
        binding.ivCall.setOnClickListener {
            call()
        }
        binding.etSearch.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                selection = binding.etSearch.text.toString()
                if (selection == "") {
                    Toast.makeText(context, "Please Enter Something for Search", Toast.LENGTH_SHORT)
                        .show()
                } else {
                    val inputMethodManager =
                        requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    inputMethodManager.hideSoftInputFromWindow(v.applicationWindowToken, 0)
                    binding.ivClear.visibility = View.VISIBLE
                    getSearchCategoryApi(selection)
                }
                return@OnEditorActionListener true
            }
            false
        })
        binding.ivClear.setOnClickListener {
            binding.ivClear.visibility = View.GONE
            binding.etSearch.setText("")
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                fragmentManager?.beginTransaction()?.detach(this)?.commitNow();
                fragmentManager?.beginTransaction()?.attach(this)?.commitNow();
            } else {
                fragmentManager?.beginTransaction()?.detach(this)?.attach(this)?.commit();
            }
        }

        binding.ivSearch.setOnClickListener(View.OnClickListener { v ->
            selection = binding.etSearch.text.toString()
            if (selection != "") {
                binding.ivClear.visibility = View.VISIBLE
                val inputMethodManager =
                    requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(v.applicationWindowToken, 0)
                getSearchCategoryApi(selection)
            } else {
                Toast.makeText(
                    activity,
                    "Please Enter Something for Search",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })

    }

    fun getServicesApi() {
        binding.shimmerViewContainer.startShimmerAnimation()
        apiClient.getApiService()
            .getService(subCategoryId, Utilities.CITYNAME)
            .enqueue(object : Callback<MainServiceResponse> {

                override fun onFailure(call: Call<MainServiceResponse>, t: Throwable) {

                    Toast.makeText(
                        activity?.applicationContext,
                        t.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()

                    binding.tvStatus.visibility = View.VISIBLE
                }

                override fun onResponse(
                    call: Call<MainServiceResponse>,
                    response: Response<MainServiceResponse>,
                ) {
                    val loginResponse = response.body()
                    if (loginResponse?.statusCode == true) {
                        list = response.body()!!.getData

                        Toast.makeText(
                            activity?.applicationContext,
                            loginResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()
                        binding.shimmerViewContainer.stopShimmerAnimation()

                        binding.shimmerViewContainer.visibility=View.GONE
                        binding.rvServices.apply {
                            setHasFixedSize(true)
                            layoutManager =
                                GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false)
//                            adapter = activity?.let { SuggestedAdapterNew(sugestedmodel, it) };
                            adapter = ServicesAdapter(
                                context as HomeActivity,
                                list!!,
                                this@ServicesFragment,
                                this@ServicesFragment
                            )

                            binding.tvStatus.visibility = View.GONE
//                            merk =
//                                response.body()!!.user.searchDataModel as ArrayList<SearchSuggestionModel>
//                            var data: MutableList<String> = ArrayList()
//                            merk.forEach {
//                                data.add(0, it.destination_name.toString())
//                            }
                            val items =
                                arrayOfNulls<String>(list!!.size)
                            for (i in 0 until list!!.size) {
                                //Storing names to string array
                                items[i] = list!!.get(i).name
                            }
                            var adapter =
                                ArrayAdapter(activity!!, R.layout.simple_list_item_1, items)
                            binding.etSearch.setAdapter(adapter)

                        }
                    } else {
//                        val loginResponse = response.body()

                        if (loginResponse != null) {
                            Toast.makeText(
                                activity?.applicationContext,
                                loginResponse.message,
                                Toast.LENGTH_SHORT
                            ).show()
                            binding.tvStatus.visibility = View.VISIBLE

                        }
                    }
                }

            })
    }

    fun getSearchCategoryApi(categoryName: String) {
        val hud = KProgressHUD.create(activity)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel("Please wait")
            .setCancellable(true)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)
            .show()

        apiClient.getApiService().getsearchApi(categoryName, Utilities.CITYNAME, domainId)
            .enqueue(object : Callback<MainServiceResponse> {
                override fun onFailure(call: Call<MainServiceResponse>, t: Throwable) {
                    hud.dismiss()
                    Toast.makeText(
                        activity?.applicationContext,
                        "failure case:   " + t.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }

                override fun onResponse(
                    call: Call<MainServiceResponse>,
                    response: Response<MainServiceResponse>,
                ) {
                    val loginResponse = response.body()

                    list = response.body()!!.getData
                    if (!list.isNullOrEmpty()) {
                        if (loginResponse?.statusCode == true) {
//                            sessionManager.saveAuthToken(loginResponse.message)
                            hud.dismiss()
                            Toast.makeText(
                                activity?.applicationContext,
                                "" + loginResponse.message,
                                Toast.LENGTH_SHORT
                            ).show()
                            binding.rvServices.apply {
                                setHasFixedSize(true)
                                layoutManager =
                                    LinearLayoutManager(
                                        activity,
                                        LinearLayoutManager.VERTICAL,
                                        false
                                    )
//                            adapter = activity?.let { SuggestedAdapterNew(sugestedmodel, it) };
                                adapter = ServicesAdapter(
                                    context as HomeActivity,
                                    list!!, this@ServicesFragment, this@ServicesFragment)

                            }

                        } else {
//                        val loginResponse = response.body()
                            hud.dismiss()
                            if (loginResponse != null) {
                                Toast.makeText(
                                    activity?.applicationContext,
                                    "" + loginResponse.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }

                    } else {
                        hud.dismiss()
                        Toast.makeText(
                            activity?.applicationContext,
                            "" + loginResponse!!.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                }
            })
    }


    private fun update(serviceid:Int,action:String) {
        val quantityy: String =
            java.lang.String.valueOf(myDatabase!!.cartDao()
                .getquantity(serviceid))
        if (action.equals("plus")){
            myDatabase!!.cartDao()
                .update((quantityy.toInt() + 1).toString(),
                    serviceid )
        }else if (action.equals("minus")){
            myDatabase!!.cartDao()
                .update((quantityy.toInt() -1).toString(),
                    serviceid )
        }else{
            myDatabase!!.cartDao()
                .update((quantityy.toInt() + 1).toString(),
                    serviceid )
        }

        getCartitems()
    }


    private fun add(
        id: Int,
        subcategoryId: String,
        name: String,
        price: String,
        description: String,
    ) {
        if (name != "" && price != "") {
            val cart = Cart(id, subcategoryId, name,
                counter, price, "prodcut_description", "productImage", "Rs ")
//            myDatabase!!.cartDao().allCartItem!![0]!!.id
            if (myDatabase!!.cartDao().allCartItem.isNullOrEmpty() || myDatabase!!.cartDao().allCartItem!![0]!!.subCategoryId.equals(
                    subcategoryId)
            ) {
//                if (Utilities.getString(activity, "subcategoryId").equals(cart.subCategoryId)) {
                AsyncTask.execute {
                    myDatabase?.cartDao()?.insertAll(cart)
                }
                Toast.makeText(activity, "Product Added to Cart", Toast.LENGTH_SHORT).show()
//                val intent = Intent(activity, PlaceMultipleOrderActivity::class.java)
                Utilities.saveString(activity, "subcategoryId", subcategoryId)
                getCartitems()
//                startActivity(intent)

            } else {

                if (!myDatabase!!.cartDao().allCartItem!![0]!!.subCategoryId.equals(subcategoryId)) {
                    showDialog("Are you sure to delete previous record?",
                        id,
                        name,
                        price,
                        subcategoryId)

//                    Toast.makeText(activity, "not same", Toast.LENGTH_SHORT).show()

                }
//                    else{
//                        Toast.makeText(activity, " same", Toast.LENGTH_SHORT).show()
//                        Toast.makeText(activity, "Product Added to Cart", Toast.LENGTH_SHORT).show()
//                        AsyncTask.execute {
//                            myDatabase?.cartDao()?.insertAll(cart)
//                        }
//                        val intent = Intent(activity, PlaceMultipleOrderActivity::class.java)
//                        Utilities.saveString(activity, "subcategoryId", subcategoryId)
//                        startActivity(intent)
//                    }

            }
//            }
//
//            }else{
//
//
//            }


        }
    }

    fun showDialog(message: String, id: Int, name: String, price: String, subcategoryId: String) {
        IOSDialog.Builder(activity)
            .title("Ustad G") // String or String Resource ID
            .message(message) // String or String Resource ID
            .positiveButtonText("Yeah, sure") // String or String Resource ID
            .negativeButtonText("No Thanks") // String or String Resource ID
            .positiveClickListener(IOSDialog.Listener { iosDialog ->

                val hud = KProgressHUD.create(activity)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setLabel("Please wait")
                    .setCancellable(true)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f)
                    .show()
                iosDialog.dismiss()


                val SPLASH_DISPLAY_LENGTH = 1500
                Handler().postDelayed({
                    AsyncTask.execute {
                        myDatabase?.cartDao()?.deleteAll()
//                        Toast.makeText(context, "cart is empty", Toast.LENGTH_SHORT).show()
                    }/* Create an Intent that will start the Menu-Activity. */
                    hud.dismiss()

                    val SPLASH_DISPLAY_LENGTH = 1000
                    Handler().postDelayed({
                        val cart = Cart(id, subcategoryId, name,
                            counter, price, "prodcut_description", "productImage", "Rs ")
                        AsyncTask.execute {
                            myDatabase?.cartDao()?.insertAll(cart)
                        }
                        Toast.makeText(activity, "new service added", Toast.LENGTH_SHORT).show()
                        getCartitems()
                        Utilities.saveString(activity, "subcategoryId", subcategoryId)

                    }, SPLASH_DISPLAY_LENGTH.toLong())
                }, SPLASH_DISPLAY_LENGTH.toLong())


            }).negativeClickListener(IOSDialog.Listener { iosDialog ->
                iosDialog.dismiss()
                Toast.makeText(context, ":(", Toast.LENGTH_SHORT).show()
            })
            .build()
            .show()
    }

    override fun onItemClicked(pos: Int) {
        val exists: Boolean = myDatabase!!.cartDao()
            .checkProduct(list!!.get(pos).id)
//        Toast.makeText(activity, list!!.get(pos).sub_category_id, Toast.LENGTH_SHORT).show()

        if (exists) {
            update(list!!.get(pos).id,"")
        } else {
            add(list!!.get(pos).id, list!!.get(pos).sub_category_id,
                list!!.get(pos).name, list!!.get(pos).price,
                "description"
            )
        }
    }

        private fun showBottomDialog(count: String, price: String) {
    //        bottomsheet = BottomSheetDialog(requireActivity())
    //        bottomsheet!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
    //        bottomsheet!!.setCancelable(true)
    //
    //        val binding: BottomSheetDialogBinding =
    //            BottomSheetDialogBinding.inflate(LayoutInflater.from(context))
    //        bottomsheet!!.setContentView(binding.getRoot())
            binding.tvCount.text = count
            binding.price.text = price



            binding.rlCountinue.setOnClickListener {

                val intent = Intent(activity, PlaceMultipleOrderActivity::class.java)
                startActivity(intent)
    //            bottomsheet!!.dismiss()

            }
    //        bottomsheet!!.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    //        bottomsheet!!.show()

        }

    private fun getCartitems() {
        total = 0.0
        itemList = java.util.ArrayList()
        itemList =
            myDatabase!!.cartDao().allCartItem as List<Cart>?
//        cartRecyclerview!!.setHasFixedSize(true)
        if (itemList!!.size > 0) {
            calculatebill()

        } else {
            binding.price.setText("Rs 0.00")
        }
    }

    private fun calculatebill() {
        for (i in 0..itemList!!.size - 1) {
            total = total + itemList!!.get(i).price.toDouble() * itemList!!.get(i).quantity
                .toDouble()
            bottomsheet?.dismiss()
//            showBottomDialog(itemList!!.size.toString(), total.toString())
            binding.tvCount.text = itemList!!.size.toString()
            binding.price.text = total.toString()

            binding.rlCountinue.setOnClickListener {

                val intent = Intent(activity, PlaceMultipleOrderActivity::class.java)
                startActivity(intent)
//            bottomsheet!!.dismiss()

            }
        }
    }

    override fun onResume() {
        super.onResume()
//        showToast("call")
    }

    fun showToast(message: String) {
        Toast.makeText(
            activity?.applicationContext,
            message,
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun call() {
        try {
            val callIntent = Intent(Intent.ACTION_CALL)
            callIntent.data = Uri.parse("tel:$03227001189")
            println("====before startActivity====")
            if (ActivityCompat.checkSelfPermission(requireActivity(),
                    Manifest.permission.CALL_PHONE) !=
                PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(requireActivity(),
                    arrayOf(Manifest.permission.CALL_PHONE),
                    MY_PERMISSIONS_REQUEST_CALL_PHONE)
                return
            }
            startActivity(callIntent)
            println("=====getcallActivity===" + getCallingActivity(requireActivity()))
        } catch (e: ActivityNotFoundException) {
            Log.e("helloAndroid", "Call failed", e)
        }
    }

    override fun onPlusClick(cartItem: ServicesDataModel?,pos: Int) {
        val exists: Boolean = myDatabase!!.cartDao()
            .checkProduct(list!!.get(pos).id)
//        Toast.makeText(activity, list!!.get(pos).sub_category_id, Toast.LENGTH_SHORT).show()

        if (exists) {
            update(list!!.get(pos).id,"plus")
        } else {
            add(list!!.get(pos).id, list!!.get(pos).sub_category_id,
                list!!.get(pos).name, list!!.get(pos).price,
                "description"
            )
        }
    }

    override fun onMinusClick(cartItem: ServicesDataModel?,pos: Int) {
        val exists: Boolean = myDatabase!!.cartDao()
            .checkProduct(list!!.get(pos).id)
//        Toast.makeText(activity, list!!.get(pos).sub_category_id, Toast.LENGTH_SHORT).show()

        if (exists) {
            update(list!!.get(pos).id,"minus")
        } else {
            add(list!!.get(pos).id, list!!.get(pos).sub_category_id,
                list!!.get(pos).name, list!!.get(pos).price,
                "description"
            )
        }


    }

    override fun onDeleteClick(cartItem: ServicesDataModel?) {
        getCartitems()

    }
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
//            override fun handleOnBackPressed() {
//           showToast("call onBackress")
//            }
//        })
//    }
//private fun getCartitems() {
//        total = 0.0
//        itemList = java.util.ArrayList()
//        itemList =
//            myDatabase!!.cartDao().allCartItem as List<Cart>?
////        cartRecyclerview!!.setHasFixedSize(true)
//        manager = LinearLayoutManager(requireContext())
//        binding.rvServices.setLayoutManager(manager)
//        val cartAdapter: ServicesAdapter = itemList?.let { ServicesAdapter(requireContext(),list,this,this) }!!
//        binding.rvServices.setAdapter(cartAdapter)
//        if (itemList!!.size > 0) {
//            calculatebill()
//        } else {
//            binding.price?.setText("Rs 0.00")
////            totaPriceee?.setText("Rs 0.00")
////            tvTotaPrice?.setText("Rs 0.00")
////            tv_text.setText("Ckeckout 0.00 $")
////            tv_min_Orderprice.setText("0.00 $")
////            tvAddMore.setVisibility(View.GONE)
//        }
//    }
}