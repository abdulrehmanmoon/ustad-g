package com.cubixsoft.mrmahircloneapp.fragments

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import com.cubixsoft.mehrancashcarry.models.HomePagerModel
import com.cubixsoft.mrmahircloneapp.HomeActivity
import com.cubixsoft.mrmahircloneapp.R
import com.cubixsoft.mrmahircloneapp.Utilities
import com.cubixsoft.mrmahircloneapp.adapters.CategoryAdapter
import com.cubixsoft.mrmahircloneapp.adapters.DealOFDayAdapter
import com.cubixsoft.mrmahircloneapp.adapters.ImageSliderAdapter
import com.cubixsoft.mrmahircloneapp.databinding.FragmentHomeBinding
import com.cubixsoft.mrmahircloneapp.models.categories.CategoryDataModel
import com.cubixsoft.mrmahircloneapp.models.categories.MainCategResponse
import com.cubixsoft.mrmahircloneapp.models.slider.MainSliderResponse
import com.cubixsoft.mrmahircloneapp.models.slider.SliderDataModel
import com.cubixsoft.mrmahircloneapp.models.trending.MainTrendingResponse
import com.cubixsoft.mrmahircloneapp.models.trending.TrendingDataModel
import com.cubixsoft.mrmahircloneapp.services.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class HomeFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {
    private lateinit var binding: FragmentHomeBinding
    lateinit var viewPager2: ViewPager2
    private lateinit var apiClient: ApiClient
    var categoryDataModel: List<CategoryDataModel>? = null
    var trendingDataModel: List<TrendingDataModel>? = null
    var sliderDDataModel: ArrayList<SliderDataModel>? = null
    private val ImageSliderHandler = Handler()
    var list: ArrayList<HomePagerModel>? = null

    @SuppressLint("UseRequireInsteadOfGet", "ResourceAsColor")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        viewPager2 = binding.viewPager222
        apiClient = ApiClient()
        val userCityName: String = Utilities.getString(activity, "userCityName")
        val domainId: String = Utilities.getString(activity, "domainId")
//        Utilities.saveString(this, "userLatitude", mLocation!!.latitude.toString())
//        Utilities.saveString(this, "userLongitude", mLocation!!.longitude.toString())

        binding.swiperefreshing.setOnRefreshListener(this)
        binding.swiperefreshing.setColorSchemeResources(R.color.colorAccent)
        setSlider()
//        getCategoryApi(userCityName)
        getCategoryApi(userCityName, domainId)
        getTrendingApi(userCityName, domainId)
        setSliderApi(userCityName, domainId)
        if (!isNetworkAvailable == true) {
            AlertDialog.Builder(activity)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Internet Connection Alert")
                .setMessage("Please Check Your Internet Connection")
                .setPositiveButton(
                    "Retry"
                ) { dialogInterface, i ->
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        fragmentManager?.beginTransaction()?.detach(this)?.commitNow();
                        fragmentManager?.beginTransaction()?.attach(this)?.commitNow();
                    } else {
                        fragmentManager?.beginTransaction()?.detach(this)?.attach(this)?.commit();
                    }
                }.show()
        } else if (isNetworkAvailable == true) {

        }
        return binding.root
    }

    val isNetworkAvailable: Boolean
        get() {
            val connectivityManager =
                activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                val capabilities =
                    connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                        return true
                    }
                }
            }
            return false
        }

    override fun onRefresh() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            fragmentManager?.beginTransaction()?.detach(this)?.commitNow();
            fragmentManager?.beginTransaction()?.attach(this)?.commitNow();
        } else {
            fragmentManager?.beginTransaction()?.detach(this)?.attach(this)?.commit();
        }
    }

//

    private val imagesliderRunable =
        Runnable { viewPager2.setCurrentItem(viewPager2.getCurrentItem() + 1) }

    fun getCategoryApi(cityName: String, domainId: String) {
        binding.shimmerViewContainer.startShimmerAnimation()
        binding.shimmerViewContainer2.startShimmerAnimation()
        apiClient.getApiService().getCategory(cityName, domainId)
            .enqueue(object : Callback<MainCategResponse> {
                override fun onFailure(call: Call<MainCategResponse>, t: Throwable) {
                    Toast.makeText(
                        activity?.applicationContext,
                        "failure case:   " + t.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()
                    binding.tvStatus.visibility = View.VISIBLE
                }

                override fun onResponse(
                    call: Call<MainCategResponse>,
                    response: Response<MainCategResponse>,
                ) {
                    val loginResponse = response.body()

                    if (loginResponse?.statusCode == true) {
                        categoryDataModel = response.body()!!.getData
                        Toast.makeText(
                            activity?.applicationContext,
                            "" + loginResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()

                        binding.rvCATEGORY.apply {
                            setHasFixedSize(true)
                            layoutManager =
                                GridLayoutManager(activity, 2, GridLayoutManager.HORIZONTAL, false)
//                            adapter = activity?.let { SuggestedAdapterNew(sugestedmodel, it) };
                            adapter = CategoryAdapter(
                                context as HomeActivity, categoryDataModel
                            )
                            binding.rvCATEGORY.setHasFixedSize(true)
                            binding.tvStatus.visibility = View.GONE

                            binding.shimmerViewContainer.stopShimmerAnimation()
                            binding.shimmerViewContainer2.stopShimmerAnimation()
                            binding.shimmerViewContainer.visibility = View.GONE
                            binding.shimmerViewContainer2.visibility = View.GONE

                        }
                    } else {
//                        val loginResponse = response.body()

                        if (loginResponse != null) {
                            Toast.makeText(
                                activity?.applicationContext,
                                "" + loginResponse.message,
                                Toast.LENGTH_SHORT
                            ).show()
                            binding.tvStatus.visibility = View.VISIBLE

                        }
                    }
                }
            })
    }

    fun getTrendingApi(cityName: String, domain: String) {
        binding.shimmerViewContainer.startShimmerAnimation()
        binding.shimmerViewContainer2.startShimmerAnimation()
        apiClient.getApiService().gettrendingApi(cityName, domain)
            .enqueue(object : Callback<MainTrendingResponse> {
                override fun onFailure(call: Call<MainTrendingResponse>, t: Throwable) {
                    Toast.makeText(
                        activity?.applicationContext,
                        "failure case:   " + t.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()
                    binding.tvStatus.visibility = View.VISIBLE
                }

                override fun onResponse(
                    call: Call<MainTrendingResponse>,
                    response: Response<MainTrendingResponse>,
                ) {
                    val loginResponse = response.body()

                    if (loginResponse?.statusCode == true) {
                        trendingDataModel = response.body()!!.getData

                        Toast.makeText(
                            activity?.applicationContext,
                            "" + loginResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()
                        binding.rvDealOfDay.apply {
                            setHasFixedSize(true)
                            layoutManager =
                                LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
//                            adapter = activity?.let { SuggestedAdapterNew(sugestedmodel, it) };
                            adapter = DealOFDayAdapter(
                                context as HomeActivity, trendingDataModel
                            )
                            binding.tvStatus.visibility = View.GONE
                            binding.shimmerViewContainer.stopShimmerAnimation()
                            binding.shimmerViewContainer2.stopShimmerAnimation()
                            binding.shimmerViewContainer.visibility = View.GONE
                            binding.shimmerViewContainer2.visibility = View.GONE
                        }
                    } else {
//                        val loginResponse = response.body()

                        if (loginResponse != null) {
                            Toast.makeText(
                                activity?.applicationContext,
                                "" + loginResponse.message,
                                Toast.LENGTH_SHORT
                            ).show()
                            binding.tvStatus.visibility = View.VISIBLE

                        }
                    }
                }
            })
    }

    fun setSliderApi(cityName: String, domain: String) {

        apiClient.getApiService().getSliderApi(cityName, domain)
            .enqueue(object : Callback<MainSliderResponse> {
                override fun onFailure(call: Call<MainSliderResponse>, t: Throwable) {

                    Toast.makeText(
                        activity?.applicationContext,
                        "failure case:   " + t.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()
                    binding.tvStatus.visibility = View.VISIBLE
                }

                override fun onResponse(
                    call: Call<MainSliderResponse>,
                    response: Response<MainSliderResponse>,
                ) {
                    val loginResponse = response.body()

                    if (loginResponse?.statusCode == true) {
                        sliderDDataModel = response.body()!!.getData

                        binding.shimmerViewContainer.visibility = View.GONE
                        binding.shimmerViewContainer2.visibility = View.GONE
                        viewPager2.adapter = ImageSliderAdapter(activity!!,
                            sliderDDataModel!!, viewPager2)
                        viewPager2.clipToPadding = false
                        viewPager2.clipChildren = false
                        viewPager2.offscreenPageLimit = 2
                        viewPager2.getChildAt(0).overScrollMode = RecyclerView.OVER_SCROLL_NEVER

                        val compositePageTransformer = CompositePageTransformer()
                        compositePageTransformer.addTransformer(MarginPageTransformer(40))
                        compositePageTransformer.addTransformer { page, position ->
                            val a = 1 - Math.abs(position)
                            page.scaleY = 0.85f + a * 0.15f
                        }

                        viewPager2.setPageTransformer(compositePageTransformer)
                        viewPager2.registerOnPageChangeCallback(object :
                            ViewPager2.OnPageChangeCallback() {
                            override fun onPageSelected(position: Int) {
                                super.onPageSelected(position)
                                ImageSliderHandler.removeCallbacks(imagesliderRunable)
                                ImageSliderHandler.postDelayed(imagesliderRunable, 3000)
                            }
                        })
                    } else {
//                        val loginResponse = response.body()

                        if (loginResponse != null) {
                            Toast.makeText(
                                activity?.applicationContext,
                                "" + loginResponse.message,
                                Toast.LENGTH_SHORT
                            ).show()
                            binding.tvStatus.visibility = View.VISIBLE

                        }
                    }
                }
            })
    }

    private fun setSlider() {
//        //Slider images
//        list = ArrayList()
//        list!!.add(HomePagerModel(R.drawable.demo3, "ipsum lorem dummy text"))
//        list!!.add(HomePagerModel(R.drawable.demo4, "ipsum lorem dummy text"))
//        list!!.add(HomePagerModel(R.drawable.demo3, "ipsum lorem dummy text"))
//        list!!.add(HomePagerModel(R.drawable.demo1, "ipsum lorem dummy text"))
//        list!!.add(HomePagerModel(R.drawable.demo2, "ipsum lorem dummy text"))
//
//        viewPager2.setAdapter(activity?.let { ImageSliderAdapter(it, list!!, viewPager2) })
//        viewPager2.setClipToPadding(false)
//        viewPager2.setClipChildren(false)
//        viewPager2.setOffscreenPageLimit(2)
//        viewPager2.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER)
//        val compositePageTransformer = CompositePageTransformer()
//        compositePageTransformer.addTransformer(MarginPageTransformer(40))
//        compositePageTransformer.addTransformer { page, position ->
//            val a = 1 - Math.abs(position)
//            page.scaleY = 0.85f + a * 0.15f
//        }
//        viewPager2.setPageTransformer(compositePageTransformer)
//        viewPager2.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
//            override fun onPageSelected(position: Int) {
//                super.onPageSelected(position)
//                ImageSliderHandler.removeCallbacks(imagesliderRunable)
//                ImageSliderHandler.postDelayed(imagesliderRunable, 3000)
//            }
//        })
    }
}