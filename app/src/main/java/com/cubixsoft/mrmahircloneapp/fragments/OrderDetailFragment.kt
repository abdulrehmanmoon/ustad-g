package com.cubixsoft.mrmahircloneapp.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import com.cubixsoft.mrmahircloneapp.Utilities
import com.cubixsoft.mrmahircloneapp.adapters.OrderDetailServicesAdapter
import com.cubixsoft.mrmahircloneapp.databinding.FragmentOrderDetailBinding
import com.cubixsoft.mrmahircloneapp.services.ApiClient
import com.myapps.healthapp1.models.subCategories.OrderServicesDataModel
import com.myapps.healthapp1.models.subCategories.PastOrderDataModel
import org.json.JSONObject




class OrderDetailFragment : Fragment() {
    private lateinit var binding: FragmentOrderDetailBinding
    private lateinit var apiClient: ApiClient
    var fragment: Fragment? = null
    var position: Int = 0
    lateinit var orderDetailList: List<PastOrderDataModel>

    @SuppressLint("UseRequireInsteadOfGet")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentOrderDetailBinding.inflate(inflater, container, false)
        apiClient = ApiClient()
        position = Utilities.getInt(requireContext(), "position")
        if (position != -1) {
            orderDetailList = Utilities.getOrderDetailServicesModel(requireContext())
            binding.recyclerview.apply {
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(requireContext())
                adapter = OrderDetailServicesAdapter(requireContext(), orderDetailList.get(position).getItems)
            }
            val  address:String=orderDetailList.get(position).getAddress.street+","+orderDetailList.get(position).getAddress.area+
                   " ,"+ orderDetailList.get(position).getAddress.city

            binding.tvAddress.text=address
            binding.tvProductName.text=orderDetailList.get(position).sub_category
            binding.tvUiSubtotal.text= orderDetailList.get(position).total_amount.toString()
            binding.tvUiTotalPrice.text= orderDetailList.get(position).total_amount.toString()
            binding.orderNumber.text="Order Number: "+orderDetailList.get(position).ordernumber
            binding.tvTimeDate.text=orderDetailList.get(position).date+","+orderDetailList.get(position).time
        }


        binding.ivBack.setOnClickListener {
            activity?.onBackPressed()
        }

        return binding.root

    }

    fun showToast(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }


}